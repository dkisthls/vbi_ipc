/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vbi.model;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.io.BufferedReader;
import java.io.BufferedWriter;

/**
 *
 * @author fwoeger
 * 
 * @param <T> should be either DspSeq or Dsp
 */
public abstract class DspTreeItem<T extends DspTreeItem> {

    private final StringProperty itemName;
    private final FloatProperty waitTime;
    private final ObservableList<T> treeItems;

    public DspTreeItem(String itemName, float waitTime) {
        // item name
        this.itemName = new SimpleStringProperty(itemName);
        // move time [s]
        this.waitTime = new SimpleFloatProperty(waitTime);
        // initialize the list
        this.treeItems = FXCollections.observableArrayList();
    }
    
    public String getItemName() {
        return itemName.get();
    }

    public void setItemName(final String itemName) {
        this.itemName.set(itemName);
    }

    public StringProperty itemNameProperty() {
        return itemName;
    }

    public float getWaitTime() {
        return waitTime.get();
    }

    public void setWaitTime(float waitTime) {
        this.waitTime.set(waitTime);
    }

    public FloatProperty waitTimeProperty() {
        return waitTime;
    }

    public ObservableList<T> getTreeItems() {
        return treeItems;
    }
    
    public abstract void add(DspTreeItem item);

    public abstract void add(int index, DspTreeItem item);
    
    public abstract float getRawDuration();
    
    public abstract float getSyncDuration();
    
    public abstract float getDataVolume();
    
    public abstract float getPeakDataRate();
    
    public abstract ObservableList<Dsp> getFlattenedTreeItems();
   
    public abstract void synchWithDspTreeItem(DspTreeItem syncWith, int syncType);
    
    public abstract void checkTreeItems();
    
    public abstract void save(BufferedWriter writer, int level);
    
    public abstract void load(BufferedReader reader, int level);
}
