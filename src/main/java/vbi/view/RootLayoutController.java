/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vbi.view;

import vbi.ipc;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Menu;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Modality;

/**
 *
 * @author fwoeger
 */
public class RootLayoutController implements Initializable {
    
    // Reference to the main application
    private ipc appIpc;

    @FXML
    Menu editMenu;
    @FXML
    CheckMenuItem expertModeCheckMenuItem;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // add listener to the expert mode check menu item
        expertModeCheckMenuItem.selectedProperty().addListener(
            (observable, oldValue, newValue) -> {
            if (newValue != null) {
                appIpc.setExpertMode(newValue);
            }
        });
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param appIpc
     */
    public void setAppIpc(ipc appIpc) {
        this.appIpc = appIpc;
    }

    /**
     * Opens a FileChooser to let the user select an address book to load.
     */
    @FXML
    private void handleOpen() throws IOException {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "CFG files (*.cfg)", "*.cfg");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show open file dialog
        File file = fileChooser.showOpenDialog(appIpc.getPrimaryStage());

        if (file != null) {
            appIpc.loadDspTreeItemDataFromFile(file);
            appIpc.setCurrentFile(file);
        }
    }

    /**
     * Saves the file to the person file that is currently open. If there is no
     * open file, the "save as" dialog is shown.
     */
    @FXML
    private void handleSave() throws IOException {
        File dspTreeItemFile = appIpc.getCurrentFile();
        if (dspTreeItemFile != null) {
            appIpc.saveDspTreeItemDataToFile(dspTreeItemFile);
        } else {
            handleSaveAs();
        }
    }

    /**
     * Opens a FileChooser to let the user select a file to save to.
     */
    @FXML
    private void handleSaveAs() throws IOException {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CFG files (*.cfg)", "*.cfg");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(appIpc.getPrimaryStage());

        if (file != null) {
            // Make sure it has the correct extension
            if (!file.getPath().endsWith(".cfg")) {
                file = new File(file.getPath() + ".cfg");
            }
            appIpc.saveDspTreeItemDataToFile(file);
            appIpc.setCurrentFile(file);
        }
    }

    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("Visible Broadband Imager\nInstrument Performance Calculator");
        alert.setContentText("Provided by the VBI Team\n\nVersion: Beta\n\nWebsite: http://dkist.nso.edu");
        alert.setResizable(true);
        alert.getDialogPane().setPrefSize(400, 240);

        alert.showAndWait();
    }

    @FXML
    private void handleQuickHelp() {
        String quickHelpText =
            "* using the 'VBI Red' and 'VBI Blue' pane, drag & drop a 'Data Set' "
          + "from the 'Data Set Library' on the right to the 'Sequence' tree "
          + "on the left to start building up a 'Sequence' for both VBI Red and "
          + "Blue, respectively\n\n"
          + "* in the individual 'VBI Red' and 'VBI Blue' panes, adjust the "
          + "'Sequence' tree on the left with the mouse context menu (right click):\n"
          + "  - create 'SubCycles', where applicable\n"
          + "  - adjust 'Cycle' and 'SubCycle' repetitions. where applicable\n"
          + "  - delete 'Data Sets' and 'SubCycles'\n\n"
          + "* edit the settings of individual 'Data Set' by selecting them in "
          + "the 'Sequence' tree, and editing parameters in the 'Settings' pane\n\n"
          + "* edit the number of fields to sample using the 'Field Selection' pane\n\n"
          + "* view the individual inteference filter profile in the context of "
          + "the solar spectrum using the 'Filter Profile' pane\n\n"
          + "* adjust the settings to synchronize the created VBI Red and Blue "
          + "channel 'Sequence' trees in the 'Sequence Timelines & "
          + "Synchronization' pane\n\n"
          + "* adjust the expected flux at the desired solar pointing using the "
          + "'Edit -> Limb Distance' menu\n\n"
          + "* switch to Expert Mode using the 'Edit -> Expert Mode' menu\n"
          + "  BEWARE: Switching back to 'normal' mode WILL delete your progress, "
          + "as invalid changes might have been made that cannot be recovered from\n"
          + "  NOTE: Expert mode enables the \"Coronal Mode\" of the IPC (currently, "
          + "Fe XI only). If the Coronal Mode is used for the Fe XI line, all other "
          + "diagnostics - even when setting the limb distance parameter for them "
          + "(it has no effect on the Fe XI SNR calculation) - will potentially "
          + "be of limited use\n\n"
          + "* monitor the 'Log Messages' pane to view automatic adjustments "
          + "made to the parameters";
        TextArea textArea = new TextArea(quickHelpText);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.initModality(Modality.NONE);
        alert.setResizable(true);
        alert.setTitle("Quick Help");
        alert.setHeaderText("Brief Usage Instructions");
        alert.getDialogPane().setPrefSize(800, 640);
        alert.getDialogPane().setContent(textArea);
        alert.show();
    }

    @FXML
    private void handleCenterDistance() {
        String centerDistanceText =
            "Please enter the mu parameter value that you would like\n"
          + "to compute the Data Set parameters for.\n"
          + "1: Disk Center\n"
          + "0: Solar Limb";

        TextInputDialog dialog = new TextInputDialog(String.valueOf(appIpc.getMu()));
        dialog.setResizable(true);
        dialog.setTitle("mu value:");
        dialog.getDialogPane().setPrefSize(550,250);
        dialog.setHeaderText(centerDistanceText);
        dialog.setContentText("mu value (0-1):");

        // The Java 8 way to get the response value (with lambda expression).
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(centerDistance -> appIpc.setMu(appIpc.checkMu(Float.parseFloat(centerDistance))));
    }
    
    @FXML
    private void handleShowEditMenu() {
        if (appIpc.getExpertMode())
          expertModeCheckMenuItem.setSelected(true);
        else
          expertModeCheckMenuItem.setSelected(false);
    }

    /**
     * Closes the application.
     */
    @FXML
    private void handleQuit() {
        System.exit(0);
    }
}    
