/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vbi;

import vbi.model.DspTreeItem;
import vbi.model.Dsp;
import vbi.model.DspGrp;
import vbi.view.IPCbasicviewController;
import vbi.view.RootLayoutController;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleFloatProperty;

/**
 *
 * @author fwoeger
 */
public class ipc extends Application {
    
    private Stage primaryStage;
    private BorderPane rootLayout;
    
    /**
     * The initialization data 
     * 1. Tree Root as DspGrp
     * 2. prefab DSPs as DspGrp
     */
    private final DspTreeItem treeBlue = new DspGrp("Cycle");
    private final DspTreeItem treeRed = new DspGrp("Cycle");
    private final DspTreeItem prefabDSPBlue = new DspGrp("PrefabDSPs");
    private final DspTreeItem prefabDSPRed = new DspGrp("PrefabDSPs");
    
    /**
     * Need to have variable for
     *   - Limb Distance Setting
     *   - Expert mode is toggled
     *   - synchronization Selection Indicator
     *   - standard file encoding
     *   - currently opened file
     */
    private final FloatProperty mu = new SimpleFloatProperty();
    private final BooleanProperty expertMode = new SimpleBooleanProperty();
    private final IntegerProperty syncSel = new SimpleIntegerProperty();
    private final BooleanProperty loadUpdateFlag = new SimpleBooleanProperty();
    final static Charset ENCODING = StandardCharsets.UTF_8;
    private File currentFile;

    /**
     * Constructor
     */
    public ipc() {
        prefabDSPBlue.add(new Dsp("Ca II K", (float)393.3, (float)10, true, false, (float)0.0));
        prefabDSPBlue.add(new Dsp("G-band", (float)430.5, (float)0.5, true, false, (float)0.0));
        prefabDSPBlue.add(new Dsp("blue cont.", (float)450.4, (float)0.2, true, false, (float)0.0));
        prefabDSPBlue.add(new Dsp("H beta", (float)486.1, (float)5, true, false, (float)0.0));
        
        prefabDSPRed.add(new Dsp("H alpha", (float)656.3, (float)2, true, false, (float)0.0));
        prefabDSPRed.add(new Dsp("red cont.", (float)668.0, (float)0.07, true, false, (float)0.0));
        prefabDSPRed.add(new Dsp("TiO", (float)705.4, (float)0.07, true, false, (float)0.0));
        prefabDSPRed.add(new Dsp("Fe XI", (float)789.2, (float)0.15, true, true, (float)0.11));
        
        expertMode.set(false);
        mu.set(1);
        syncSel.set(0);
        loadUpdateFlag.set(false);
        currentFile = null;
    }

    /**
     * 
     * @param primaryStage
     * @throws Exception 
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("VBI IPC");
        
        initRootLayout();
        
        showIPCBasicOverview();
    }
    
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            // Give the controller access to the main app.
            RootLayoutController controller = loader.getController();
            controller.setAppIpc(this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
  
    /**
     * Shows the IPC overview inside the root layout.
     */
    public void showIPCBasicOverview() {
        try {
            // Load IPC overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(this.getClass().getResource("view/IPCbasicview.fxml"));
            AnchorPane ipcView = (AnchorPane) loader.load();

            // Set IPC overview into the center of root layout.
            rootLayout.setCenter(ipcView);
            
            // Give the controller access to the main app
            IPCbasicviewController bvcontroller = loader.getController();
            bvcontroller.setIPCApp(this);
            // run post-init
            // basically: create a listener on
            // - the Expert Mode
            // - the Disk Center Distance
            bvcontroller.postInit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }
  
    /**
     * Functions to get back and set the currentFile name
     * @return currentFile - current file being processed, if any
     */
    public File getCurrentFile() {
        return currentFile;
    }
    public void setCurrentFile(File currentFile) {
        this.currentFile = currentFile;
    }
    /**
     * Functions to get back and set Expert mode flag
     * @return expertMode - flag indicating whether expert mode is selected
     */
    public boolean getExpertMode() {
        return expertMode.get();
    }
    public BooleanProperty getExpertModeProperty() {
        return expertMode;
    }
    public void setExpertMode(boolean expertMode) {
        this.expertMode.set(expertMode);
    }
    /**
     * Functions to get back and set synchronization selection indicator
     * @return syncSel - indicator for selected synchronization
     */
    public int getSyncSel() {
        return syncSel.get();
    }
    public IntegerProperty getSyncSelProperty() {
        return syncSel;
    }
    public void setSyncSel(int syncSel) {
        this.syncSel.set(syncSel);
    }
    /**
     * Functions to get back and set mu
     * @return mu - distance from center in linear coords between one and zero
     */
    public float getMu() {
        return mu.get();
    }
    public FloatProperty getMuProperty() {
        return mu;
    }
    public void setMu(float mu) {
        this.mu.set(mu);
    }
    public float checkMu(float mu) {
        float goodMu = mu;
        if (mu < 0.0) goodMu = (float)0.0;
        if (mu > 1.0) goodMu = (float)1.0;
        return goodMu;
    }
    /**
     * Functions to get back and set load update flag
     * @return loadUpdateFlag - flag indicating whether a file was loaded
     */
    public boolean getLoadUpdateFlag() {
        return loadUpdateFlag.get();
    }
    public BooleanProperty getLoadUpdateFlagProperty() {
        return loadUpdateFlag;
    }
    public void setLoadUpdateFlag(boolean loadUpdateFlag) {
        this.loadUpdateFlag.set(loadUpdateFlag);
    }
    
    /**
     * Loads Dsp Sequence items from file
     * 
     * @param fileName
     * @throws java.io.IOException
     */
    public void loadDspTreeItemDataFromFile(File fileName) throws IOException {
      InputStreamReader inStream = null;
      BufferedReader reader;
      String line;
      String[] kvPair;

      try {
        inStream = new InputStreamReader(new FileInputStream(fileName), ENCODING);
        treeBlue.getTreeItems().clear();
        treeRed.getTreeItems().clear();
      } catch (FileNotFoundException e1) {
        Logger.getLogger(ipc.class.getName()).log(Level.SEVERE, null, e1);
      }
      
      reader = new BufferedReader(inStream);
      while ((line = reader.readLine()) != null) {
          if (line.contains("--- General")) {
            kvPair = line.split("=");
            // only do this until the last item shows up
            // in this case 'syncSel'
            while (!kvPair[0].contentEquals("syncSel")) { 
              line = reader.readLine();
              kvPair = line.split("=");
              switch (kvPair[0]) {
                case("mu"):
                  this.setMu(Float.parseFloat(kvPair[1]));
                  break;
                case("expertMode"):
                  this.setExpertMode(Boolean.parseBoolean(kvPair[1]));
                  break;
                case("syncSel"):
                  this.setSyncSel(Integer.parseInt(kvPair[1]));
                  break;
                default:
                  break;
              }
            }
          } else if (line.contains("--- VBIBLUE")) {
              treeBlue.getTreeItems().clear();
              line = reader.readLine();
              kvPair = line.split("=");
              if (kvPair[0].contains("LEVEL") && kvPair[1].contentEquals("0"))
                  treeBlue.load(reader, Integer.parseInt(kvPair[1]));
          } else if (line.contains("--- VBIRED")) {
              treeRed.getTreeItems().clear();
              line = reader.readLine();
              kvPair = line.split("=");
              if (kvPair[0].contains("LEVEL") && kvPair[1].contentEquals("0"))
                  treeRed.load(reader, Integer.parseInt(kvPair[1]));
          }
      }
      
      // make sure the Listener is triggered.
      this.setLoadUpdateFlag(false);
      this.setLoadUpdateFlag(true);

      this.setCurrentFile(fileName);
    }

    /**
     * Saves the current person data to the specified file.
     * 
     * @param fileName
     * @throws java.io.IOException
     */
    public void saveDspTreeItemDataToFile(File fileName) throws IOException {
      OutputStreamWriter outStream = null;
      BufferedWriter writer;

      try {
        outStream = new OutputStreamWriter(new FileOutputStream(fileName), ENCODING);
      } catch (FileNotFoundException e1) {
        Logger.getLogger(ipc.class.getName()).log(Level.SEVERE, null, e1);
      }

      writer = new BufferedWriter(outStream);
      writer.write("--- General"+"\n");
      writer.write("mu="+Float.toString(this.getMu())+"\n");
      writer.write("expertMode="+Boolean.toString(this.getExpertMode())+"\n");
      writer.write("syncSel="+Integer.toString(this.getSyncSel())+"\n");
      writer.write("--- VBIBLUE"+"\n");
      treeBlue.save(writer, 0);
      writer.write("--- END"+"\n");
      writer.write("--- VBIRED"+"\n");
      treeRed.save(writer, 0);
      writer.write("--- END"+"\n");
      writer.close();
    }
    
    // >>>>> BLUE
    /**
     * returns the data as an observable list of DSPs
     * @return prefbDSPs
     */
    public DspTreeItem getDspPrefabRootBlue() {
        return prefabDSPBlue;
    }
    public DspTreeItem getTRootBlue() {
        return treeBlue;
    }
    // >>>>> RED
    /**
     * returns the data as an observable list of DSPs
     * @return prefbDSPs
     */
    public DspTreeItem getDspPrefabRootRed() {
        return prefabDSPRed;
    }
    public DspTreeItem getTRootRed() {
        return treeRed;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
