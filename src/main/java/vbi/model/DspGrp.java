/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vbi.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;

/**
 *
 * @author fwoeger
 */
public class DspGrp extends DspTreeItem<DspTreeItem> {
    public final IntegerProperty numCycles;
    
    /**
     *  Default Constructor
     */
    public DspGrp() {
        this("");
    }
    
    /**
     * Constructor with some initial data.
     * 
     * @param itemName Name of item
     * 
     */
    public DspGrp(String itemName) {
        super(itemName, 0);
        this.numCycles = new SimpleIntegerProperty(1);
    }
    
    public int getNumCycles() {
        return this.numCycles.get();
    }

    public void setNumCycles(final int numCycles) {
        this.numCycles.set(numCycles);
    }

    public IntegerProperty numCyclesProperty() {
        return this.numCycles;
    }
    
    public void equalizeCadence() {
        ObservableList<Dsp> list = this.getFlattenedTreeItems();
        int lengthList = list.size();
        
        // find maximum duration
        float maxDspDuration = (float)0.0;
        for (int i=0; i<lengthList; i++) {
            if (list.get(i).getRawDuration() > maxDspDuration)
                maxDspDuration = list.get(i).getRawDuration();
        }
        // set the waitTime of each Dsp to make it last maxSingleDspDuration
        for (int i=0; i<lengthList; i++) {
            list.get(i).setWaitTime(maxDspDuration - list.get(i).getRawDuration());
        }
    }

    @Override
    public void add(DspTreeItem item) {
        getTreeItems().add(item);
    }

    @Override
    public void add(int index, DspTreeItem item) {
        getTreeItems().add(index, item);
    }

    @Override
    public float getRawDuration() {
        float accumulator = (float)0.0;
        float cycles = (float)this.numCycles.get();
        int numberOfItems;
        
        numberOfItems = this.getTreeItems().size();
        
        if (numberOfItems > 0) {
            for (int i=0; i<numberOfItems; i++) {
                DspTreeItem temp = this.getTreeItems().get(i);
                if (temp instanceof Dsp) {
                    accumulator += ((Dsp)(temp)).getRawDuration();
                } else if (temp instanceof DspGrp) {
                    accumulator += ((DspGrp)(temp)).getRawDuration();
                }
            }
        } else {
            accumulator = (float)0.0;
        }

        return (cycles*accumulator);
    }

    @Override
    public float getSyncDuration() {
        float accumulator = (float)0.0;
        float cycles = (float)this.numCycles.get();
        int numberOfItems;
        
        numberOfItems = this.getTreeItems().size();
        
        if (numberOfItems > 0) {
            for (int i=0; i<numberOfItems; i++) {
                DspTreeItem temp = this.getTreeItems().get(i);
                if (temp instanceof Dsp) {
                    accumulator += ((Dsp)(temp)).getSyncDuration();
                } else if (temp instanceof DspGrp) {
                    accumulator += ((DspGrp)(temp)).getSyncDuration();
                }
            }
        } else {
            accumulator = (float)0.0;
        }

        return (cycles*accumulator);
    }

    @Override
    public float getDataVolume() {
        float accumulator = (float)0.0;
        float cycles = (float)this.numCycles.get();
        int numberOfItems;
        
        numberOfItems = this.getTreeItems().size();
        
        if (numberOfItems > 0) {
            for (int i=0; i<numberOfItems; i++) {
                DspTreeItem temp = this.getTreeItems().get(i);
                if (temp instanceof Dsp) {
                    accumulator += ((Dsp)(temp)).getDataVolume();
                } else if (temp instanceof DspGrp) {
                    accumulator += ((DspGrp)(temp)).getDataVolume();
                }
            }
        } else {
            accumulator = (float)0.0;
        }

        return (cycles*accumulator);
    }

    @Override
    public float getPeakDataRate() {
        float peakDataRate = (float)0.0;
        int numberOfItems;
        
        numberOfItems = this.getTreeItems().size();
        
        if (numberOfItems > 0) {
            for (int i=0; i<numberOfItems; i++) {
                DspTreeItem temp = this.getTreeItems().get(i);
                if (temp instanceof Dsp) {
                    float tPDR = ((Dsp)(temp)).getPeakDataRate();
                    if (tPDR > peakDataRate) peakDataRate = tPDR;
                } else if (temp instanceof DspGrp) {
                    float tPDR = ((DspGrp)(temp)).getPeakDataRate();
                    if (tPDR > peakDataRate) peakDataRate = tPDR;
                }
            }
        } else {
            peakDataRate = (float)0.0;
        }

        return (peakDataRate);
    }
    
    @Override
    public ObservableList<Dsp> getFlattenedTreeItems() {
        ObservableList<Dsp> output = FXCollections.observableArrayList();
        int numberOfItems;
        
        numberOfItems = this.getTreeItems().size();

        if (numberOfItems > 0) {
            for (int i=0; i<numberOfItems; i++) {
                DspTreeItem temp = this.getTreeItems().get(i);
                if (temp instanceof Dsp) {
                    output.addAll(((Dsp)temp).getFlattenedTreeItems());
                } else if (temp instanceof DspGrp) {
                    for (int j=0; j<((DspGrp)temp).getNumCycles(); j++) {
                        output.addAll(((DspGrp)temp).getFlattenedTreeItems());
                    }
                }
            }
        }
        
        return output;
    }
    
    /**
     * Sync with another DspTreeItem
     * @param syncWith - DspTreeItem to sync with
     * @param syncType - 0: None, 1: Loose, 2: Fixed
     */
    @Override
    public void synchWithDspTreeItem(DspTreeItem syncWith, int syncType) {
        // clear cycle wait time
        this.setWaitTime(0);
        // clear the extra times on this Tree (Dsp is the leaf)
        for (int i=0; i<this.getTreeItems().size(); i++) {
            this.getTreeItems().get(i).synchWithDspTreeItem(syncWith, syncType);
        }
        switch (syncType) {
            case 0:
                // clearing waitTimes was already done above
                break;
            case 1:
                // get the durations for each Tree, divide by the numCycles that
                // are otherwise included
                // use getRawDuration (instead of getSyncDuration), because
                // we want to be efficient
                float durationA = this.getRawDuration() / (float)this.getNumCycles();
                float durationB = (float)0.0;
                if (syncWith != null) {
                    if (syncWith instanceof DspGrp) {
                        durationB = syncWith.getRawDuration() / ((DspGrp)syncWith).getNumCycles();
                    } else {
                        durationB = syncWith.getRawDuration();
                    }
                }
                // if this Tree is shorter, then we have to make it longer by
                // adding a delay at the end of last Dsp
                if (durationA < durationB) {
                    this.setWaitTime(durationB-durationA);
                } else if ((durationB > 0.0) && (durationA > durationB)) {
                    syncWith.setWaitTime(durationA-durationB);
                }
                break;
            case 2:
                // equalize cadence of this
                this.equalizeCadence();
                // if we have something to compare against
                if (syncWith != null) {
                    syncWith.setWaitTime(0);
                    if (syncWith instanceof DspGrp) ((DspGrp)syncWith).equalizeCadence();

                    ObservableList<Dsp> listA = this.getFlattenedTreeItems();
                    ObservableList<Dsp> listB = syncWith.getFlattenedTreeItems();

                    // now cross-reference if applicable
                    for (int i=0; i<Math.max(listA.size(), listB.size()); i++) {
                        // list item always a Dsp
                        listA.get(i%listA.size()).synchWithDspTreeItem(listB.get(i%listB.size()), syncType);
                    }
                }
                break;
            default:
                break;
        }
    }
    
    @Override
    public void checkTreeItems() {
      ObservableList<Dsp> list = this.getFlattenedTreeItems();
      if (list.size() == 1)
        list.get(0).checkTreeItems();
      else {
        for (int i=0; i<list.size(); i++) {
              list.get(i).setMoveTime(Dsp.MOVETIME);
        }
      }
    }

    @Override
    public void save(BufferedWriter writer, int level) {
      int numberOfItems = this.getTreeItems().size();

      try {
        writer.write("LEVEL="+Integer.toString(level)+"\n");
        writer.write("type=DspGroup"+"\n");
        writer.write("itemName="+this.getItemName()+"\n");
        writer.write("waitTime="+Float.toString(this.getWaitTime())+"\n");
        writer.write("numCycles="+Integer.toString(this.getNumCycles())+"\n");
        if (numberOfItems > 0) {
            for (int i=0; i<numberOfItems; i++) {
                DspTreeItem temp = this.getTreeItems().get(i);
                if (temp instanceof Dsp) {
                    ((Dsp)(temp)).save(writer, level+1);
                } else if (temp instanceof DspGrp) {
                    ((DspGrp)(temp)).save(writer, level+1);
                }
            }
        } else {
            writer.write("end"+"\n");
        }
      } catch (IOException e1) {
          Logger.getLogger(DspGrp.class.getName()).log(Level.SEVERE, null, e1);
      }
    }
    
    @Override
    public void load(BufferedReader reader, int level) {
        String line;
        String[] kvPair;

        try {
          // this is a DspGrp item, so read the DspGrp item stuff
          while (((line = reader.readLine()) != null) && !line.contentEquals("--- END")) {
              kvPair = line.split("=");
              if (kvPair[0].contentEquals("type")) {
                if (kvPair[1].contentEquals("DspGroup")) {
                  // only do this until the last item shows up
                  // in this case 'numCycles'
                  while (!kvPair[0].contentEquals("numCycles")) { 
                    line = reader.readLine();
                    kvPair = line.split("=");
                    switch (kvPair[0]) {
                      case ("itemName"):
                        this.setItemName(kvPair[1]);
                        break;
                      case("waitTime"):
                        this.setWaitTime(Float.parseFloat(kvPair[1]));
                        break;
                      case("numCycles"):
                        this.setNumCycles(Integer.parseInt(kvPair[1]));
                        break;
                      default:
                        break;
                    }
                  }
                  // MARK 1 - we need this because if we are at the 'wrong'
                  // level we need to read the next line again.
                  // Because we are in DspGrp object, the next line must be
                  // LEVEL.
                  reader.mark(512);
                  line = reader.readLine();
                  kvPair = line.split("=");
                }
              }
              // just checking:
              if (kvPair[0].contentEquals("LEVEL")) {
                int tlevel = Integer.parseInt(kvPair[1]);
                // if the read-in level is higher, then we need to go lower.
                // this is the easy stuff: create new objects as necessary.
                if (tlevel > level) {
                  // MARK 2: need to mark this line - it needs to be read
                  // again when instantiating the object later.
                  // However, we also need to read it now, because we need
                  // to know what new object to create.
                  reader.mark(256);
                  line = reader.readLine();
                  kvPair = line.split("=");
                  if (kvPair[0].contentEquals("type")) {
                    if (kvPair[1].contentEquals("DspGroup")) {
                      this.add(new DspGrp());
                    } else if (kvPair[1].contentEquals("Dsp")) {
                      this.add(new Dsp());
                    }
                    // Reset to MARK 2
                    reader.reset();
                    // recursively read in the next object, after we got
                    // ready for it with the previous line
                    this.getTreeItems().get(this.getTreeItems().size()-1).load(reader, tlevel);
                  }
                } else {
                  // reset to MARK 1
                  reader.reset();
                  break;
                }
              }
            }
        } catch (IOException ex) {
            Logger.getLogger(DspGrp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
