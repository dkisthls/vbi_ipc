/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vbi.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author fwoeger
 */
public final class Flux {

  // Set expected character encoding in file.
  final static Charset ENCODING = StandardCharsets.UTF_8;

  // constants
  static long   c_light  = 299792458;
  static double h_planck = 6.62606957E-34;
  static double d_sun    = 32.*60.;
  static double pi       = 3.141592653589793;
  static double d_tel    = 4.0;

  // Data for optical components
  ArrayList<Double> wavelength = new ArrayList<>();
  ArrayList<Double> spectrum = new ArrayList<>();
  ArrayList<Double> atmosphere = new ArrayList<>();
  ArrayList<Double> aluminum = new ArrayList<>();
  ArrayList<Double> fss06 = new ArrayList<>();
  ArrayList<Double> fss45 = new ArrayList<>();
  ArrayList<Double> infrasil = new ArrayList<>();
  ArrayList<Double> refloss = new ArrayList<>();
  ArrayList<Double> camera = new ArrayList<>();

  // Data for Limb Darkening
  ArrayList<Double> ldWavelength = new ArrayList<>();
  ArrayList<Double> ldA0 = new ArrayList<>();
  ArrayList<Double> ldA1 = new ArrayList<>();
  ArrayList<Double> ldA2 = new ArrayList<>();
  ArrayList<Double> ldA3 = new ArrayList<>();
  ArrayList<Double> ldA4 = new ArrayList<>();
  ArrayList<Double> ldA5 = new ArrayList<>();

  // Static and Final Array
  ArrayList<Double> intFil;
  ArrayList<Double> staticSpectrum = new ArrayList<>();
  ArrayList<Double> finalSpectrum = new ArrayList<>();
  
  /**
   * Constructor for the class
   * @param filter
   * @throws IOException 
   */
  public Flux(int filter) throws IOException {
    String FilterString = String.valueOf(filter);

    try {
      this.readData(
          this.getClass().getResourceAsStream(FilterString.concat(".txt")),
          this.wavelength, this.spectrum, this.atmosphere, this.aluminum,
          this.fss06, this.fss45, this.infrasil, this.refloss, this.camera
      );
      this.readData(
          this.getClass().getResourceAsStream("limbcoefficients.txt"),
          this.ldWavelength,
          this.ldA0, this.ldA1, this.ldA2, this.ldA3, this.ldA4, this.ldA5
      );
    } catch(IOException error) {
      System.out.println(Arrays.toString(error.getStackTrace()));
    }
    this.applyConfiguration(
      this.getClass().getResourceAsStream("VBI".concat(FilterString.concat(".txt")))
    );

    // 393.3 filter
    //   in this case, the FIDO optics is simulated as two reflection loss surfaces
    //   each VBI filter is treated separately, with its own parameters
//    this.applyCustomConfiguration("refloss", "refloss", "interferencefilter 0.5 393.3 0.1 2.0");
//    this.photonCalculation(Math.pow(0.011, 2), 10., 393.3, 0.0);

    // 430.5 filter
    //   in this case, the FIDO optics is simulated as two reflection loss surfaces
    //   each VBI filter is treated separately, with its own parameters
//    this.applyCustomConfiguration("refloss", "refloss", "interferencefilter 0.7 430.5 0.4 2.0");
//    this.photonCalculation(Math.pow(0.011, 2), 0.5, 430.5, 0.0);
  }

  // Trapez Integration of values. Nothing fancy.
  //   x:   ordinate
  //   y:   abscissa
  //   cut: do not use the last 'cut' values in integration
  private double trapezIntegrate(ArrayList<Double> x, ArrayList<Double> y, int cut) {
    double output = 0.0;

    long end = x.size();

    // trapez rule:
    for (int i=1; i<(end-cut); i++) {
      output += Math.abs(x.get(i)-x.get(i-1))*(y.get(i)+y.get(i-1));
    }
    //output += Math.abs(x.get((int)(end-1-cut)))*(y.get((int)(end-1-cut)));
    output *= 0.5;

    return output;
  }

  // Interpolation for reflectivities of FSS99-500 for arbitrary angles
  //   angle: angle of incidence on FSS99 coated substrate in [deg]
  private ArrayList<Double> interpolateFSS99(double angle) {
    
    ArrayList<Double> output = new ArrayList<>();
    
    if ((angle > 6.0) && (angle < 45.0)) {
      for (int i=0; i<this.wavelength.size(); i++) {
        double val = (1 - Math.pow((angle-6.0)/(45.0-6.0), 2)) * this.fss06.get(i) +
                          Math.pow((angle-6.0)/(45.0-6.0), 2)  * this.fss45.get(i);
        if (val < 0.0) { val = 0.0; }
        if (val > 1.0) { val = 0.0; }

        output.add(val);
      }
    } else if (angle <= 6.0) {
      for (int i=0; i<this.wavelength.size(); i++) {
        output.add(this.fss06.get(i));
      }
    } else if (angle >= 45.0) {
      for (int i=0; i<this.wavelength.size(); i++) {
        output.add(this.fss45.get(i));
      }
    }
    
    return output;
  }

  // Beer-Lambert law, using extinction (given in [1/cm])
  //   thickness: thickness of substrate in [cm]
  private ArrayList<Double> infrasilTransmission(double thickness) {
    ArrayList<Double> output = new ArrayList<>();

    for (int i=0; i<this.wavelength.size(); i++) {
      output.add(Math.exp(-this.infrasil.get(i)*thickness));
    }
    return output;
  }

  // Ideal Interference Filter curve with
  //   CWL:      Central Wavelength of bandpass filter in [nm]
  //   FWHM:     Full Width Half Maximum of bandpass filter [nm]
  //   Cavities: Number of cavities in filter
  private ArrayList<Double> interferenceFilter(double transmission, double CWL, double FWHM, double Cavities) {
    ArrayList<Double> output = new ArrayList<>();
    double maximum = -1.0;
    double sigma   = 0.5 * FWHM;

    // Lorentzian curve
    for (int i=0; i<this.wavelength.size(); i++) {
      double filter = 1.0 / (1.0 + Math.pow((this.wavelength.get(i) - CWL) / sigma, 2.0*Cavities));
      if (filter > maximum) maximum = filter;
      output.add(filter);
    }
    // Normalize and apply transmission coefficient
    for (int i=0; i<this.wavelength.size(); i++) {
      double temp = output.get(i);
      output.set(i, transmission * temp / maximum);
    }

    return output;
  }

  // Limb darkening computation
  //   lambda: wavelength for which to calculate limb darkening
  //   mu: distance from disk center
  //       to compute with radius = linearDistanceFromDiskCenter / Rsun:
  //       mu = cos(asin(radius)), 'cosine of viewing angle'
  // The Neckel & Labs [1994] coefficients are for a polynomial of 5th order in mu
  private double limbDarkening(double lambda, double mu) {
    double output;
    int index = 0;

    // compute mu [unnecessary since we changed to provide mu directly for consistency with other tools]
    //double mu = Math.cos(Math.asin(radius));

    // find best parameters for given wavelength (assumes monotonically
    // increase in ld.Wavelength)
    for (int i=0; i<this.ldWavelength.size()-1; i++) {
      if (lambda >= this.ldWavelength.get(i)) { index = i+1; }
    }
    // interpolate, where it makes sense
    if ((index > 0) && (lambda < this.ldWavelength.get(this.ldWavelength.size()-1))) {
      double y0 = this.ldA0.get(index-1)*Math.pow(mu,0) +
                  this.ldA1.get(index-1)*Math.pow(mu,1) +
                  this.ldA2.get(index-1)*Math.pow(mu,2) + 
                  this.ldA3.get(index-1)*Math.pow(mu,3) + 
                  this.ldA4.get(index-1)*Math.pow(mu,4) + 
                  this.ldA5.get(index-1)*Math.pow(mu,5);
      double y1 = this.ldA0.get(index)*Math.pow(mu,0) +
                  this.ldA1.get(index)*Math.pow(mu,1) +
                  this.ldA2.get(index)*Math.pow(mu,2) + 
                  this.ldA3.get(index)*Math.pow(mu,3) + 
                  this.ldA4.get(index)*Math.pow(mu,4) + 
                  this.ldA5.get(index)*Math.pow(mu,5);
      double t  = (lambda - this.ldWavelength.get(index-1)) /
                  (this.ldWavelength.get(index) - this.ldWavelength.get(index-1));
      output    = (1.0-t)*y0 + t*y1;
    } else {
      output = this.ldA0.get(index)*Math.pow(mu,0) +
               this.ldA1.get(index)*Math.pow(mu,1) +
               this.ldA2.get(index)*Math.pow(mu,2) + 
               this.ldA3.get(index)*Math.pow(mu,3) + 
               this.ldA4.get(index)*Math.pow(mu,4) + 
               this.ldA5.get(index)*Math.pow(mu,5);
    }
    return output;
  }

  private void readData(InputStream fileStream, ArrayList<Double>... vars) throws IOException {

    BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream, ENCODING));
    int outputArrayNumber = vars.length;

    double newVal;

    try {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] columns = line.split("\\s+");

        // If the first column is preceded by white spaces, then we have one
        // column too many (the first one being bogus), so we have to watch out:
        int offset;
        if (columns[0].matches("^\\s*$")) {offset = 1;} else {offset = 0;}

        // If there aren't as many values as input arrays, we can skip the line
        if ((columns.length-offset) != outputArrayNumber) { continue; }

        for (int i=0; i<outputArrayNumber; i++) {
          try {
            newVal = Double.parseDouble(columns[i + offset]);
          } catch (NumberFormatException e1) {
            continue;
          }
          vars[i].add(newVal);
        }
      }
    } catch (IOException e2) {
        System.out.println(Arrays.toString(e2.getStackTrace()));
    } finally {
      try {
        reader.close();
      } catch (IOException e3) {
          System.out.println(Arrays.toString(e3.getStackTrace()));
      }
    }
  }

  private void applyConfiguration(InputStream fileStream) throws IOException {

    BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream, ENCODING));

    // clear previous ...
    this.staticSpectrum.clear();
    // ... and initialize with solar spectrum
    for (int i=0; i<this.wavelength.size(); i++) {
      // solar spectrum will set up the array
      this.staticSpectrum.add(i, this.spectrum.get(i));
    }

    try {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] columns = line.split("\\s+");

        if (columns[0].matches("atmosphere")) {
          for (int i=0; i<this.wavelength.size(); i++) {
            this.staticSpectrum.set(i, this.staticSpectrum.get(i)*this.atmosphere.get(i));
          }
          continue;
        }
        if (columns[0].matches("aluminum")) {
          for (int i=0; i<this.wavelength.size(); i++) {
            this.staticSpectrum.set(i, this.staticSpectrum.get(i)*this.aluminum.get(i));
          }
          continue;
        }
        if (columns[0].matches("fss99")) {
          double angle = Double.parseDouble(columns[1]);
          ArrayList<Double> fssInterp = interpolateFSS99(angle);
          for (int i=0; i<this.wavelength.size(); i++) {
            this.staticSpectrum.set(i, this.staticSpectrum.get(i)*fssInterp.get(i));
          }
          continue;
        }
        if (columns[0].matches("infrasil")) {
          double thickness = Double.parseDouble(columns[1]);
          ArrayList<Double> infrasilTransmissionForThickness = infrasilTransmission(thickness);
          for (int i=0; i<this.wavelength.size(); i++) {
            this.staticSpectrum.set(i, this.staticSpectrum.get(i)*infrasilTransmissionForThickness.get(i));
          }
          continue;
        }
        if (columns[0].matches("refloss")) {
          for (int i=0; i<this.wavelength.size(); i++) {
            this.staticSpectrum.set(i, this.staticSpectrum.get(i)*refloss.get(i));
          }
          continue;
        }
        if (columns[0].matches("interferencefilter")) {
          double trans= Double.parseDouble(columns[1]);
          double cwl  = Double.parseDouble(columns[2]);
          double fwhm = Double.parseDouble(columns[3]);
          double cavs = Double.parseDouble(columns[4]);
          this.intFil = interferenceFilter(trans, cwl, fwhm, cavs);
          for (int i=0; i<this.wavelength.size(); i++) {
            this.staticSpectrum.set(i, this.staticSpectrum.get(i)*this.intFil.get(i));
          }
          continue;
        }
        if (columns[0].matches("camera")) {
          for (int i=0; i<this.wavelength.size(); i++) {
            this.staticSpectrum.set(i, this.staticSpectrum.get(i)*camera.get(i));
          }
          continue;
        }
        if (columns[0].matches("flat")) {
          double flatVal = Double.parseDouble(columns[1]);
          for (int i=0; i<this.wavelength.size(); i++) {
            this.staticSpectrum.set(i, this.staticSpectrum.get(i)*flatVal);
          }
          continue;
        }
      }
    } catch (IOException e2) {
        System.out.println(Arrays.toString(e2.getStackTrace()));
    } finally {
      try {
        reader.close();
      } catch (IOException e3) {
          System.out.println(Arrays.toString(e3.getStackTrace()));
      }
    }
    // now create the finalSpectrum copy
    //   clear final spectrum (just to make sure)
    this.finalSpectrum.clear();
    //   add the values
    for (int i=0; i<this.wavelength.size(); i++) {
      this.finalSpectrum.add(i, this.staticSpectrum.get(i));
    }
  }

  public void applyCustomConfiguration(String... customOptics) {

    // clear final spectrum
    this.finalSpectrum.clear();
    // copy static to final spectrum
    for (int i=0; i<this.wavelength.size(); i++) {
      this.finalSpectrum.add(this.staticSpectrum.get(i));
    }

    /* Parse and apply the customOptics arguments */
    for (String customOptic : customOptics) {
      String[] columns = customOptic.split("\\s+");
      if (columns[0].matches("aluminum")) {
        for (int i=0; i<this.wavelength.size(); i++) {
          this.finalSpectrum.set(i, this.finalSpectrum.get(i)*this.aluminum.get(i));
        }
        continue;
      }
      if (columns[0].matches("fss99")) {
        double angle = Double.parseDouble(columns[1]);
        ArrayList<Double> fssInterp = interpolateFSS99(angle);
        for (int i=0; i<this.wavelength.size(); i++) {
          this.finalSpectrum.set(i, this.finalSpectrum.get(i)*fssInterp.get(i));
        }
        continue;
      }
      if (columns[0].matches("infrasil")) {
        double thickness = Double.parseDouble(columns[1]);
        ArrayList<Double> infraS = infrasilTransmission(thickness);
        for (int i=0; i<this.wavelength.size(); i++) {
          this.finalSpectrum.set(i, this.finalSpectrum.get(i)*infraS.get(i));
        }
        continue;
      }
      if (columns[0].matches("refloss")) {
        for (int i=0; i<this.wavelength.size(); i++) {
          this.finalSpectrum.set(i, this.finalSpectrum.get(i)*refloss.get(i));
        }
        continue;
      }
      if (columns[0].matches("interferencefilter")) {
        double trans= Double.parseDouble(columns[1]);
        double cwl  = Double.parseDouble(columns[2]);
        double fwhm = Double.parseDouble(columns[3]);
        double cavs = Double.parseDouble(columns[4]);
        this.intFil = interferenceFilter(trans, cwl, fwhm, cavs);
        for (int i=0; i<this.wavelength.size(); i++) {
          this.finalSpectrum.set(i, this.finalSpectrum.get(i)*this.intFil.get(i));
        }
        continue;
      }
      if (columns[0].matches("camera")) {
        for (int i=0; i<this.wavelength.size(); i++) {
          this.finalSpectrum.set(i, this.finalSpectrum.get(i)*camera.get(i));
        }
        continue;
      }
      if (columns[0].matches("flat")) {
        double flatVal = Double.parseDouble(columns[1]);
        for (int i=0; i<this.wavelength.size(); i++) {
          this.finalSpectrum.set(i, this.finalSpectrum.get(i)*flatVal);
        }
        continue;
      }
    }
  }

  public ArrayList<Double> getWavelength() {
      return this.wavelength;
  }
  
  public ArrayList<Double> getSpectrum() {
      return this.spectrum;
  }
  
  public ArrayList<Double> getIntFil() {
      return this.intFil;
  }
  
  public double photonPerSecCalculation(double FOV, double lambda, double radialDistance) {
    DecimalFormat myFormatter = new DecimalFormat("0.#####E0");
      
    ArrayList<Double> Irradiance = new ArrayList<>();
    for (int i=0; i<this.wavelength.size(); i++) {
      Irradiance.add(i, this.finalSpectrum.get(i)*this.wavelength.get(i)*1e-9);
    }

    double tel_collect_area = Flux.pi * Math.pow(Flux.d_tel/2., 2.0);

    double fov_ratio = FOV / (Flux.pi * Math.pow(Flux.d_sun/2., 2.0));

    double integSpectralIrradiance = trapezIntegrate(this.wavelength, this.finalSpectrum, 2);
    double integIrradiance = trapezIntegrate(this.wavelength, Irradiance, 2);

    double limbDark = limbDarkening(lambda, radialDistance);

    // various outputs
    double watts = (tel_collect_area*fov_ratio)*limbDark*integSpectralIrradiance;
    double photons_per_second = (tel_collect_area*fov_ratio)/(Flux.c_light*Flux.h_planck)*limbDark*integIrradiance;

/*
    System.out.format("Wavelength: %f nm%n", lambda);
    System.out.format("Watts impeding on FOV: " + myFormatter.format(watts) + "%n");
    System.out.format("Photons impeding on FOV per second: " + myFormatter.format(photons_per_second) + "%n");
    double photons = photons_per_second*exposTimeMillisec/(double)1000.;
    System.out.format("Total number of photons impeding on FOV within exposure: " + myFormatter.format(photons) + "%n");
    double SNR = Math.sqrt(photons);
    System.out.format("Signal to Noise Ratio, assuming photon noise: " + myFormatter.format(SNR) + "%n");
    System.out.println();
*/

    return photons_per_second;
  }
}
