/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vbi.model;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Model class for a DSP
 * 
 * @author fwoeger
 */
public class Dsp extends DspTreeItem<DspTreeItem> {
    public static final int     NUMFIELDS = 9;
    public static final int     NUMROIPARAMS = 4;
    public static final long    FULLWELL = 50000;
    public static final float   CAMREADNOISE = 12;
    public static final long    MAXDATARATE = 960;
    public static final int     MAXPIXELX = 4096;
    public static final int     MAXPIXELY = 4096;
    public static final float   MOVETIME = (float)0.533;
    public static final float   CONFTIME = (float)1.2;
    public static final int     DEFAULTFRAMERATE = 30;
    public static final int     DEFAULTNUMSPECKLEIMS = 80;
    public static final int     MINNUMIMS = 1;
    public static final int     MAXNUMIMS = 10;
    public static final boolean DEFAULTISCORONAL = false;

    private Flux fluxInfo = null;

    private final FloatProperty   waveLength;
    private final BooleanProperty isCoronalWavelength;
    private final FloatProperty   relCoronalFlux;
    private final FloatProperty   relCoronalBackground;
    private final FloatProperty   coronalLineWidth;
    private final FloatProperty   expTime;
    private final BooleanProperty isLib;
    private final FloatProperty   frameRate;
    private final IntegerProperty numAcqImages;
    private final IntegerProperty numSelImages;
    private final IntegerProperty binFactor;
    private final BooleanProperty speckleProcessing;
    private final BooleanProperty frameSelection;
    private final FloatProperty   moveTime;
    private final FloatProperty   confTime;
    private final ListProperty<BooleanProperty> fieldSamples;
    private final ListProperty<IntegerProperty> roiParams;

    /**
     *  Default Constructor
     */
    public Dsp() {
        this(null, 0, 0, false, false, 0);
    }
    
    /**
     * Constructor with some initial data.
     * 
     * @param itemName - Name of Dsp
     * @param waveLength - Wavelength of DSP in [nm]
     * @param expTime - Exposure time for DSP in [ms]
     * @param libFlag - Flag indicating whether this is a library (unmodifiable)
     *                  item
     * @param coronalWavelengthFlag - flag indicating whether this wavelength
     *                                can be meaningfully observed in the corona
     * @param coronalLineWidth - flag for coronal line width in [nm]
     * 
     */
    public Dsp(String itemName, float waveLength, float expTime, boolean libFlag, boolean coronalWavelengthFlag, float coronalLineWidth) {
        // Name of DSP (name of line, e.g. Ca II K)
        super(itemName, 0);
        // Wavelength [nm]
        this.waveLength = new SimpleFloatProperty(waveLength);
        this.isCoronalWavelength = new SimpleBooleanProperty(coronalWavelengthFlag);
        this.relCoronalFlux = new SimpleFloatProperty((float)0.0);
        this.relCoronalBackground = new SimpleFloatProperty((float)0.0);
        this.coronalLineWidth = new SimpleFloatProperty(coronalLineWidth);
        try {
            // create Flux object at this point, if we can.
            if (this.getWaveLength() != 0) {
              this.fluxInfo = new Flux((int)this.getWaveLength());
            }
        } catch (IOException e1) {
            Logger.getLogger(Dsp.class.getName()).log(Level.SEVERE, null, e1);
        }
        // Exposure Time [ms]
        this.expTime = new SimpleFloatProperty(expTime);

        // Initialized default parameters
        // is this a library Dsp?
        this.isLib = new SimpleBooleanProperty(libFlag);
        // Frame Rate [Hz]
        this.frameRate = new SimpleFloatProperty(Dsp.DEFAULTFRAMERATE);
        // number of images to acquire
        this.numAcqImages = new SimpleIntegerProperty(Dsp.DEFAULTNUMSPECKLEIMS);
        // number of images to select
        this.numSelImages = new SimpleIntegerProperty(Dsp.DEFAULTNUMSPECKLEIMS);
        // binning factor (applies both in horizontal and vertical pixels)
        this.binFactor = new SimpleIntegerProperty(1);
        // speckle processing flag
        this.speckleProcessing = new SimpleBooleanProperty(true);
        // frame selection flag
        this.frameSelection = new SimpleBooleanProperty(false);
        // initialize move time, Dsp.MOVETIME by default
        this.moveTime = new SimpleFloatProperty(Dsp.MOVETIME);
        // initialize move time, Dsp.MOVETIME by default
        this.confTime = new SimpleFloatProperty(Dsp.CONFTIME);
        // field n
        this.fieldSamples = new SimpleListProperty<>(FXCollections.observableArrayList(new ArrayList<>()));
        for (int i=0; i<NUMFIELDS; i++) {
            this.fieldSamples.add(new SimpleBooleanProperty(false));
        }
        this.fieldSamples.set(4, new SimpleBooleanProperty(true));
        // ROI parameters (lower left x, lower left y, width, height)
        this.roiParams = new SimpleListProperty<>(FXCollections.observableArrayList(new ArrayList<>()));
        for (int i=0; i<NUMROIPARAMS; i++) {
            this.roiParams.add(new SimpleIntegerProperty(0));
        }
        this.setRoiParams(0, new SimpleIntegerProperty(0));
        this.setRoiParams(1, new SimpleIntegerProperty(0));
        this.setRoiParams(2, new SimpleIntegerProperty(Dsp.MAXPIXELX));
        this.setRoiParams(3, new SimpleIntegerProperty(Dsp.MAXPIXELY));
    }

    public final float getWaveLength() {
        return waveLength.get();
    }

    public final void setWaveLength(float waveLength) {
        this.waveLength.set(waveLength);
    }

    public final FloatProperty waveLengthProperty() {
        return waveLength;
    }

    public final boolean getIsCoronalWavelength() {
        return isCoronalWavelength.get();
    }

    public final void setIsCoronalWavelength(boolean isCoronalWavelength) {
        this.isCoronalWavelength.set(isCoronalWavelength);
    }

    public final BooleanProperty IsCoronalWavelengthProperty() {
        return isCoronalWavelength;
    }

    public final float getRelCoronalFlux() {
        return relCoronalFlux.get();
    }

    public final void setRelCoronalFlux(float relCoronalFlux) {
        this.relCoronalFlux.set(relCoronalFlux);
    }

    public final FloatProperty relCoronalFlux() {
        return relCoronalFlux;
    }

    public final float getRelCoronalBackground() {
        return relCoronalBackground.get();
    }

    public final void setRelCoronalBackground(float relCoronalBackground) {
        this.relCoronalBackground.set(relCoronalBackground);
    }

    public final FloatProperty relCoronalBackground() {
        return relCoronalBackground;
    }

    public final float getCoronalLineWidth() {
        return coronalLineWidth.get();
    }

    public final void setCoronalLineWidth(float coronalLineWidth) {
        this.coronalLineWidth.set(coronalLineWidth);
    }

    public final FloatProperty coronalLineWidth() {
        return coronalLineWidth;
    }

    public final float getExpTime() {
        return expTime.get();
    }

    public final void setExpTime(float expTime) {
        this.expTime.set(expTime);
    }

    public final FloatProperty expTimeProperty() {
        return expTime;
    }

    public final boolean getIsLib() {
        return isLib.get();
    }

    public final void setIsLib(boolean isLib) {
        this.isLib.set(isLib);
    }

    public final BooleanProperty isLibProperty() {
        return isLib;
    }

    public final float getFrameRate() {
        return frameRate.get();
    }

    public final void setFrameRate(float frameRate) {
        this.frameRate.set(frameRate);
    }

    public final FloatProperty frameRateProperty() {
        return frameRate;
    }

    public final int getNumAcqImages() {
        return numAcqImages.get();
    }

    public final void setNumAcqImages(int numAcqImages) {
        this.numAcqImages.set(numAcqImages);
    }

    public final IntegerProperty numAcqImagesProperty() {
        return numAcqImages;
    }

    public final int getNumSelImages() {
        return numSelImages.get();
    }

    public final void setNumSelImages(int numSelImages) {
        this.numSelImages.set(numSelImages);
    }

    public final IntegerProperty numSelImagesProperty() {
        return numSelImages;
    }

    public final int getBinFactor() {
        return binFactor.get();
    }

    public final void setBinFactor(int binFactor) {
        this.binFactor.set(binFactor);
    }

    public final IntegerProperty binFactorProperty() {
        return binFactor;
    }

    public final boolean getSpeckleProcessing() {
        return speckleProcessing.get();
    }

    public final void setSpeckleProcessing(boolean speckleProcessing) {
        this.speckleProcessing.set(speckleProcessing);
    }

    public final BooleanProperty speckleProcessingProperty() {
        return speckleProcessing;
    }

    public final boolean getFrameSelection() {
        return frameSelection.get();
    }

    public final void setFrameSelection(boolean frameSelection) {
        this.frameSelection.set(frameSelection);
    }

    public final BooleanProperty frameSelectionProperty() {
        return frameSelection;
    }

    public final float getMoveTime() {
        return moveTime.get();
    }

    public final void setMoveTime(float moveTime) {
        this.moveTime.set(moveTime);
    }

    public final FloatProperty moveTimeProperty() {
        return moveTime;
    }

    public final float getConfTime() {
        return confTime.get();
    }

    public final void setConfTime(float confTime) {
        this.confTime.set(confTime);
    }

    public final FloatProperty confTimeProperty() {
        return confTime;
    }

    public final BooleanProperty getFieldSamples(int i) {
        if (i<Dsp.NUMFIELDS) {
            return fieldSamples.get(i);
        } else return null;
    }

    public final int getNumActiveFieldSamples() {
        int result = 0;
        for (int i=0; i<Dsp.NUMFIELDS; i++) {
            if (fieldSamples.get(i).get() == true) {
                result += 1;
            }
        }
        return result;
    }

    public final void setFieldSamples(int i, BooleanProperty fieldSample) {
        if (i<Dsp.NUMFIELDS) {
            this.fieldSamples.set(i, fieldSample);
        }
    }

    public final ObservableList<BooleanProperty> fieldSamplesProperty() {
        return fieldSamples;
    }    

    public final IntegerProperty getRoiParams(int i) {
        return roiParams.get(i);
    }

    public final void setRoiParams(int i, IntegerProperty roiParams) {
        this.roiParams.set(i, roiParams);
    }

    public final ObservableList<IntegerProperty> roiParamsProperty() {
        return roiParams;
    }

    public final Flux getFluxInfo() {
        return fluxInfo;
    }
    
    /** compute photon flux under various situations
     * 
     * @param pixArea - pixel area size in [arcs**2]
     * @param mu - cos(theta), theta: latitude with zero defined along Earth-Sun axis
     *             disk center: mu = 1
     *             solar limb:  mu = 0
     * @param lineMult - indicate whether line is coronal or not
     * 
     * @return
     */
    public final double getPhotonsPerSecond(float pixArea, float mu, boolean lineMult) {
        double photonsPerSecond;
        if (this.getIsCoronalWavelength() & (this.getRelCoronalFlux() > 0.0)) {
          if (lineMult) {
              // assume a coronal line width has simple Lorentzian profile
              // ignore radial distance because the input intensity values are relative
              // to disk center
              this.fluxInfo.applyCustomConfiguration(
                    "interferencefilter 1.0 " + this.waveLength.getValue().toString() +
                    " " + this.coronalLineWidth.getValue().toString() + " 1.0"
              );
          }
          photonsPerSecond =
                this.fluxInfo.photonPerSecCalculation(pixArea, this.getWaveLength(), 0.0);
          // clear custom configuration
          this.fluxInfo.applyCustomConfiguration("");
        } else {
          photonsPerSecond =
                this.fluxInfo.photonPerSecCalculation(pixArea, this.getWaveLength(), mu);
        }
        return photonsPerSecond;
    }
    
    /** ROI verification check
     * 
     * @param inputROIString - string of 4 numbers,
     *         indicating lower left x and y of pixel, and width and height of 
     *         ROI
     * @return - a valid specification of an ROI
     */
    public final int[] checkROIValue(String[] inputROIString) {
        int[] goodROIValues = new int[Dsp.NUMROIPARAMS];
   
        boolean inputOK = true;

        if (inputROIString.length == Dsp.NUMROIPARAMS) {
            goodROIValues[0] = Integer.parseInt(inputROIString[0].trim());
            goodROIValues[1] = Integer.parseInt(inputROIString[1].trim());
            goodROIValues[2] = Integer.parseInt(inputROIString[2].trim());
            goodROIValues[3] = Integer.parseInt(inputROIString[3].trim());
            
            if ((goodROIValues[0] < 0) || (goodROIValues[1] < 0)) inputOK = false;
            if ((goodROIValues[0] > Dsp.MAXPIXELX) || (goodROIValues[1] > Dsp.MAXPIXELY)) inputOK = false;
            if ((goodROIValues[2] < 0) || (goodROIValues[3] < 0)) inputOK = false;
            if ((goodROIValues[2] > Dsp.MAXPIXELX) || (goodROIValues[3] > Dsp.MAXPIXELY)) inputOK = false;
            if ((goodROIValues[2]-goodROIValues[0] < 0) || (goodROIValues[3]-goodROIValues[1] < 0)) inputOK = false;
            if ((goodROIValues[2]+goodROIValues[0] > Dsp.MAXPIXELX) || (goodROIValues[3]+goodROIValues[1] > Dsp.MAXPIXELY)) inputOK = false;
        } else {
            inputOK = false;
        }
        
        // make values binning factor compatible
        if (inputOK) {
            goodROIValues[2] = (goodROIValues[2]/this.getBinFactor())*this.getBinFactor();
            goodROIValues[3] = (goodROIValues[3]/this.getBinFactor())*this.getBinFactor();
        }
        
        //  ROI won't work with more than one field sample
        if (this.getNumActiveFieldSamples() != 1) {
            inputOK = false;
        }
        // ROI won't work with speckle
        if (this.getSpeckleProcessing()){
            inputOK = false;
        }
        
        if (!inputOK) {
            // always resets to the full chip ROI (or what is compatible with 
            // the current bin factor) if the user made an error
            goodROIValues[0] = 0;
            goodROIValues[1] = 0;
            goodROIValues[2] = (Dsp.MAXPIXELX/this.getBinFactor())*this.getBinFactor();
            goodROIValues[3] = (Dsp.MAXPIXELY/this.getBinFactor())*this.getBinFactor();
        }

        return goodROIValues;
    }

    /** Bin verification check
     * 
     * @param inputBinFactor - integer of desired (square) binning factor
     * @return - a valid specification of a bin factor
     */
    public final int checkBinningFactor(int inputBinFactor) {
        int goodBinFactor = inputBinFactor;
        
        if (goodBinFactor < 1) goodBinFactor = 1;
        if (goodBinFactor > 4) goodBinFactor = 4;
        
        if (this.getSpeckleProcessing()) goodBinFactor = 1;

        return goodBinFactor;
    }

    /** Exposure time and exposure rate compatibility
     * 
     * @param inputExpTime - exposure time to be checked
     * @return goodExposureTime - an exposure time compatible with the selected Frame Rate
     */
    public final float checkExposureTimeVsFrameRate(float inputExpTime) {
        float goodExposureTime = inputExpTime/(float)1000.;

        if (goodExposureTime > (float)1./this.getFrameRate()) goodExposureTime = (float)1./this.getFrameRate();
        
        return goodExposureTime*(float)1000.;
    }

    /** Exposure time and exposure rate compatibility
     * 
     * @param inputExpTime - exposure time to be tested in ms
     * @param pixArea - area of the pixel
     * @param radDist - distance from sun center
     * @return goodExposureTime - an exposure time compatible with the Full Well capacity of the camera
     */
    public final float checkExposureTimeVsFullWell(float inputExpTime, float pixArea, float radDist) {
        float goodExposureTime = inputExpTime;
        
        double photons = this.getPhotonsPerSecond(pixArea, radDist, true);
        
        if (this.getIsCoronalWavelength() & (this.getRelCoronalFlux() > 0.0)) {
          photons *= inputExpTime*1.0e-3 * (this.getRelCoronalFlux() + this.getRelCoronalBackground())*1.0e-6;
        } else {
          photons *= inputExpTime*1.0e-3;
        }

        // any necessary custom configuration was already applied or cleared
        if (photons > Dsp.FULLWELL) {
          if (this.getIsCoronalWavelength() & (this.getRelCoronalFlux() > 0.0)) {
            goodExposureTime =
                (float)(Dsp.FULLWELL /
                    ((this.getRelCoronalFlux() + this.getRelCoronalBackground())*1e-6 *
                      this.getPhotonsPerSecond(pixArea, radDist, true)) * 1.0e3);
          } else {
            goodExposureTime =
                (float)(Dsp.FULLWELL / this.fluxInfo.photonPerSecCalculation(pixArea, this.getWaveLength(), radDist) * 1.0e3);
          }
        }

        return goodExposureTime;
    }
    
    /** Exposure time and exposure rate compatibility
     * 
     * @param inputFrameRate
     * @return goodFrameRate - a frame rate that makes sense
     */
    public final float checkFrameRate(float inputFrameRate) {
        float goodFrameRate = inputFrameRate;
        
        // Multiply by 2 for 16bit data, and convert to MB
        // CMOS:
        // the speedup in framerate is only along one direction, lets say rows
        // binning will not result in speedup, as each pixel is always read out
        double dataSize = 
//          (this.getRoiParams(2).get()*this.getRoiParams(3).get())
            (this.getRoiParams(2).get()*Dsp.MAXPIXELY)
//          / (this.getBinFactor()*this.getBinFactor())
            * (double)2.0 / Math.pow(1024., 2.0);

        if ((dataSize*inputFrameRate) > Dsp.MAXDATARATE) {
            goodFrameRate = (float)((double)Dsp.MAXDATARATE / dataSize);
        }
        
        if (goodFrameRate > (float)(1./this.getExpTime()*1.0e3)) {
          goodFrameRate = (float)(1./this.getExpTime()*1.0e3);
        }

        return goodFrameRate;
    }

    /** Check that selected and acquired number of images make sense with the
     * selection
     * 
     * @param inputNumSelIms - input number of selected images
     * @param inputNumAcqIms - input number of acquired images
     * @return output - an array of 2 coherent values for selected and acquired images
     */
    public final int[] checkNumImages(int inputNumSelIms, int inputNumAcqIms) {
        int goodNumAcqIms = inputNumAcqIms;
        int goodNumSelIms = inputNumSelIms;
        int[] output = new int[2];

        if (this.getFrameSelection()) {
            if (this.getSpeckleProcessing()) {
                if (inputNumSelIms < Dsp.DEFAULTNUMSPECKLEIMS)
                    goodNumSelIms = Dsp.DEFAULTNUMSPECKLEIMS;
                if (inputNumAcqIms < goodNumSelIms)
                    goodNumAcqIms = goodNumSelIms;
            } else {
                if (inputNumSelIms > Dsp.MAXNUMIMS)
                    goodNumSelIms = Dsp.MAXNUMIMS;
                else if (inputNumSelIms < Dsp.MINNUMIMS)
                    goodNumSelIms = Dsp.MINNUMIMS;
                if (inputNumAcqIms < goodNumSelIms)
                    goodNumAcqIms = goodNumSelIms;
            }
        } else {
            if (this.getSpeckleProcessing()) {
                if (inputNumAcqIms < Dsp.DEFAULTNUMSPECKLEIMS)
                    goodNumAcqIms = Dsp.DEFAULTNUMSPECKLEIMS;
                if (inputNumSelIms != goodNumAcqIms)
                    goodNumSelIms = goodNumAcqIms;
            } else {
                if (inputNumAcqIms > Dsp.MAXNUMIMS)
                    goodNumAcqIms = Dsp.MAXNUMIMS;
                else if (inputNumAcqIms < Dsp.MINNUMIMS)
                    goodNumAcqIms = Dsp.MINNUMIMS;
                if (inputNumSelIms != goodNumAcqIms)
                    goodNumSelIms = goodNumAcqIms;
            }
        }
       
        output[0] = goodNumSelIms;
        output[1] = goodNumAcqIms;

        return output;
    }
    
    /**
     * Create a copy of the Dsp
     * a straight copy can never be isLib = true
     * 
     * @return copyOfThis - a copy of this Dsp
     */
    public final Dsp copy() {
        Dsp copyOfThis = new Dsp();

        copyOfThis.setItemName(this.getItemName());
        copyOfThis.setWaveLength(this.getWaveLength());
        copyOfThis.setIsCoronalWavelength(this.getIsCoronalWavelength());
        copyOfThis.setRelCoronalFlux(this.getRelCoronalFlux());
        copyOfThis.setRelCoronalBackground(this.getRelCoronalBackground());
        copyOfThis.setCoronalLineWidth(this.getCoronalLineWidth());
        copyOfThis.setExpTime(this.getExpTime());
        copyOfThis.setIsLib(false);
        copyOfThis.setFrameRate(this.getFrameRate());
        copyOfThis.setWaitTime(this.getWaitTime());
        copyOfThis.setConfTime(this.getConfTime());
        copyOfThis.setNumAcqImages(this.getNumAcqImages());
        copyOfThis.setNumSelImages(this.getNumSelImages());
        copyOfThis.setSpeckleProcessing(this.getSpeckleProcessing());
        copyOfThis.setFrameSelection(this.getFrameSelection());
        copyOfThis.setBinFactor(this.getBinFactor());
        for (int i=0; i<NUMFIELDS; i++) {
            copyOfThis.fieldSamples.set(i, this.getFieldSamples(i));
        }
        for (int i=0; i<NUMROIPARAMS; i++) {
            copyOfThis.roiParams.set(i, this.getRoiParams(i));
        }
        try {
            copyOfThis.fluxInfo = new Flux((int)this.getWaveLength());
        } catch (IOException ex) {
            Logger.getLogger(Dsp.class.getName()).log(Level.SEVERE, null, ex);
        }

        return copyOfThis;
    }
    
    @Override
    public ObservableList<DspTreeItem> getTreeItems() {
        return FXCollections.emptyObservableList();
    }

    @Override
    public void add(DspTreeItem item) {
        throw new IllegalStateException("Dsp has no child items");
    }
    
    @Override
    public void add(int index, DspTreeItem item) {
        throw new IllegalStateException("Dsp has no child items");
    }

    @Override
    public float getRawDuration() {
        float duration;

        float nFields = (float)getNumActiveFieldSamples();
        float fRate   = (float)getFrameRate();
        float nIms    = (float)getNumAcqImages();
        float mTime   = (float)getMoveTime();
        float cTime   = (float)getConfTime();
        
        // configuration time for camera masks one move time contained in the
        // nFields product
        duration = nFields*(nIms/fRate + mTime) + (cTime - mTime);
        
        return duration;
    }

    @Override
    public float getSyncDuration() {
        float duration;

        float nFields = (float)getNumActiveFieldSamples();
        float fRate   = (float)getFrameRate();
        float nIms    = (float)getNumAcqImages();
        float mTime   = (float)getMoveTime();
        float cTime   = (float)getConfTime();
        float wTime   = (float)getWaitTime();
        
        // configuration time for camera masks one move time contained in the
        // nFields product
        duration = nFields*(nIms/fRate + mTime) + (cTime - mTime) + wTime;

        return duration;
    }

    @Override
    public float getDataVolume() {
        float volume;

        float nFields = (float)getNumActiveFieldSamples();
        float nIms    = (float)getNumSelImages();
        int   binFac  = (int)getBinFactor();
        int   sizeX   = (int)getRoiParams(2).get() - (int)getRoiParams(0).get();
        int   sizeY   = (int)getRoiParams(3).get() - (int)getRoiParams(1).get();
        
        if (getSpeckleProcessing()) {
            // factor 2 is for 16 bit data
            volume = 2*nFields*(float)sizeX/(float)binFac*(float)sizeY/(float)binFac;
        } else {
            // factor 2 is for 16 bit data
            volume = 2*nFields*nIms*(float)sizeX/(float)binFac*(float)sizeY/(float)binFac;           
        }

        return volume;
    }

    @Override
    public float getPeakDataRate() {
        float fRate   = (float)getFrameRate();
        int   binFac  = (int)getBinFactor();
        int   sizeX   = (int)getRoiParams(2).get() - (int)getRoiParams(0).get();
        int   sizeY   = (int)getRoiParams(3).get() - (int)getRoiParams(1).get();
        // factor 2 is for 16 bit data
        return (2*(float)sizeX/(float)binFac*(float)sizeY/(float)binFac*fRate);
    }
    
    @Override
    public ObservableList<Dsp> getFlattenedTreeItems() {
        ObservableList<Dsp> output = FXCollections.observableArrayList();
        
        output.add(this);
        
        return output;
    }
    
     /**
     * Sync with another DspTreeItem
     * @param syncWith - DspTreeItem to sync with
     * @param syncType - 0: None, 1: Loose, 2: Fixed
     */
    @Override
    public void synchWithDspTreeItem(DspTreeItem syncWith, int syncType) {
        // always need to reset the waitTime
        switch(syncType) {
            case 0:
                this.setWaitTime(0);
                break;
            case 1:
                this.setWaitTime(0);
                break;
            case 2:
                // total duration of this Dsp
                float timeA = this.getSyncDuration();
                if (syncWith != null) {
                    // total duration of the Dsp to synch with
                    float timeB = syncWith.getSyncDuration();
                    if (timeA < timeB)
                        this.setWaitTime(timeB - this.getRawDuration());
                    else
                        syncWith.setWaitTime(timeA - syncWith.getRawDuration());
                } else {
                    this.setWaitTime(0);
                }
                break;
            default:
                break;       
        }
    }

    @Override
    public void checkTreeItems() {
      if ((this.getNumActiveFieldSamples() == 1) && !(this.getSpeckleProcessing())) {
        this.setMoveTime(0);
        this.setConfTime(0);
      } else {
        this.setMoveTime(Dsp.MOVETIME);
        this.setConfTime(Dsp.CONFTIME);
      }
    }
        
    @Override
    public void save(BufferedWriter writer, int level) {
      try {
        writer.write("LEVEL="+Integer.toString(level)+"\n");
        writer.write("type=Dsp"+"\n");
        writer.write("itemName="+this.getItemName()+"\n");
        writer.write("waitTime="+Float.toString(this.getWaitTime())+"\n");
        writer.write("waveLength="+Float.toString(this.getWaveLength())+"\n");
        writer.write("isCoronalWavelength="+Boolean.toString(this.getIsCoronalWavelength())+"\n");
        writer.write("relCoronalFlux="+Float.toString(this.getRelCoronalFlux())+"\n");
        writer.write("relCoronalBackground="+Float.toString(this.getRelCoronalBackground())+"\n");
        writer.write("coronalLineWidth="+Float.toString(this.getCoronalLineWidth())+"\n");
        writer.write("expTime="+Float.toString(this.getExpTime())+"\n");
        writer.write("frameRate="+Float.toString(this.getFrameRate())+"\n");
        writer.write("numAcqImages="+Integer.toString(this.getNumAcqImages())+"\n");
        writer.write("numSelImages="+Integer.toString(this.getNumSelImages())+"\n");
        writer.write("binFactor="+Integer.toString(this.getBinFactor())+"\n");
        writer.write("speckleProcessing="+Boolean.toString(this.getSpeckleProcessing())+"\n");
        writer.write("frameSelection="+Boolean.toString(this.getFrameSelection())+"\n");
        writer.write("moveTime="+Float.toString(this.getMoveTime())+"\n");
        writer.write("confTime="+Float.toString(this.getConfTime())+"\n");
        writer.write("fieldSamples=");
          for (int i=0; i<Dsp.NUMFIELDS-1; i++) writer.write(Boolean.toString(this.getFieldSamples(i).get())+",");
          writer.write(Boolean.toString(this.getFieldSamples(Dsp.NUMFIELDS-1).get())+"\n");
        writer.write("roiParams=");
          for (int i=0; i<Dsp.NUMROIPARAMS-1; i++) writer.write(Integer.toString(this.getRoiParams(i).get())+",");
          writer.write(Integer.toString(this.getRoiParams(Dsp.NUMROIPARAMS-1).get())+"\n");
      } catch (IOException e1) {
        Logger.getLogger(DspGrp.class.getName()).log(Level.SEVERE, null, e1);
      }

    }
    
    @Override
    public void load(BufferedReader reader, int level) {
        String line;
        String[] kvPair;
        String[] temp;

        try {
            line = reader.readLine();
            kvPair = line.split("=");
            if (kvPair[0].contentEquals("type") && kvPair[1].contentEquals("Dsp")) {
              // only do this until the last item shows up
              // in this case 'roiParams'
              while (!kvPair[0].contentEquals("roiParams")) { 
                line = reader.readLine();
                kvPair = line.split("=");
                switch (kvPair[0]) {
                  case ("itemName"):
                      this.setItemName(kvPair[1]);
                      break;
                  case("waitTime"):
                      this.setWaitTime(Float.parseFloat(kvPair[1]));
                      break;
                  case("waveLength"):
                      this.setWaveLength(Float.parseFloat(kvPair[1]));
                      break;
                  case("isCoronalWavelength"):
                      this.setIsCoronalWavelength(Boolean.parseBoolean(kvPair[1]));
                      break;
                  case("relCoronalFlux"):
                      this.setRelCoronalFlux(Float.parseFloat(kvPair[1]));
                      break;
                  case("relCoronalBackground"):
                      this.setRelCoronalBackground(Float.parseFloat(kvPair[1]));
                      break;
                  case("coronalLineWidth"):
                      this.setCoronalLineWidth(Float.parseFloat(kvPair[1]));
                      break;
                  case("expTime"):
                      this.setExpTime(Float.parseFloat(kvPair[1]));
                      break;
                  case("frameRate"):
                      this.setFrameRate(Float.parseFloat(kvPair[1]));
                      break;
                  case("numAcqImages"):
                      this.setNumAcqImages(Integer.parseInt(kvPair[1]));
                      break;
                  case("numSelImages"):
                      this.setNumSelImages(Integer.parseInt(kvPair[1]));
                      break;
                  case("binFactor"):
                      this.setBinFactor(Integer.parseInt(kvPair[1]));
                      break;
                  case("speckleProcessing"):
                      this.setSpeckleProcessing(Boolean.parseBoolean(kvPair[1]));
                      break;
                  case("frameSelection"):
                      this.setFrameSelection(Boolean.parseBoolean(kvPair[1]));
                      break;
                  case("moveTime"):
                      this.setMoveTime(Float.parseFloat(kvPair[1]));
                      break;
                  case("confTime"):
                      this.setConfTime(Float.parseFloat(kvPair[1]));
                      break;
                  case("fieldSamples"):
                      temp = kvPair[1].split(",");
                      for (int i=0; i<Dsp.NUMFIELDS; i++)
                        this.getFieldSamples(i).set(Boolean.parseBoolean(temp[i]));
                      break;
                  case("roiParams"):
                      temp = kvPair[1].split(",");
                      for (int i=0; i<Dsp.NUMROIPARAMS; i++)
                        this.getRoiParams(i).set(Integer.parseInt(temp[i]));
                      break;
                  default:
                      break;
                }
              }

              this.setIsLib(false);
              try {
                  // create Flux object at this point, if we can.
                  if (this.getWaveLength() != 0) this.fluxInfo = new Flux((int)this.getWaveLength());
              } catch (IOException e1) {
                  Logger.getLogger(Dsp.class.getName()).log(Level.SEVERE, null, e1);
              }
            }
        } catch (IOException ex) {
            Logger.getLogger(DspGrp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
