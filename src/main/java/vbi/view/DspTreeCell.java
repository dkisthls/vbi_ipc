/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vbi.view;

import vbi.model.DspTreeItem;
import vbi.model.DspGrp;
import vbi.model.Dsp;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;


/**
 *
 * @author fwoeger
 */
public class DspTreeCell extends TreeCell<DspTreeItem> {

    private static TreeItem<DspTreeItem> draggedTreeItem = null;
    private static TreeView<DspTreeItem> draggedTreeView = null;
    
    private enum DropType { DROP_INTO, DROP_INTO_EMPTY, MOVE_ABOVE, MOVE_BELOW };
    private static DropType dropType;

    private final ContextMenu cMenuDsp = new ContextMenu();
    private final ContextMenu cMenuDspGrp = new ContextMenu();
    
    private TextField textField;
    
    // set up the context menus here
    public DspTreeCell() {
        // DspGrp Context Menu
        MenuItem cMenuAddDspGrp = new MenuItem("Add a SubCycle");
        cMenuDspGrp.getItems().add(cMenuAddDspGrp);
        cMenuAddDspGrp.setOnAction(new EventHandler() {
            public void handle(Event event) {
                DspTreeItem newDspGrp = new DspGrp("Cycle");
                TreeItem newDspGrpTreeItem = new TreeItem<>(newDspGrp);
                DspTreeCell.this.getTreeItem().getChildren().add(newDspGrpTreeItem);
                buildModelFromView(DspTreeCell.this.getTreeView().getRoot(), DspTreeCell.this.getTreeView().getRoot().getValue());
                // adjust move time in a DSP, if needed
                DspTreeCell.this.getTreeView().getRoot().getValue().checkTreeItems();

                // trigger listener to update the view
                triggerListener();
            }
        });
        MenuItem cMenuNumCycles = new MenuItem("Change Number of Cycles");
        cMenuDspGrp.getItems().add(cMenuNumCycles);
        cMenuNumCycles.setOnAction(new EventHandler() {
            public void handle(Event event) {
                DspTreeCell.this.startEdit();
                buildModelFromView(DspTreeCell.this.getTreeView().getRoot(), DspTreeCell.this.getTreeView().getRoot().getValue());
                // adjust move time in a DSP, if needed
                DspTreeCell.this.getTreeView().getRoot().getValue().checkTreeItems();
                // trigger listener to update the view not required because
                // it is in the createTextField() which is called by startEdit()
            }
        });
        MenuItem cMenuDeleteGrp = new MenuItem("Delete");
        cMenuDspGrp.getItems().add(cMenuDeleteGrp);
        cMenuDeleteGrp.setOnAction(new EventHandler() {
            public void handle(Event event) {
                if (!(DspTreeCell.this.getItem().equals(DspTreeCell.this.getTreeView().getRoot().getValue()))) {
                    DspTreeCell.this.getTreeItem().getParent().getChildren().remove(DspTreeCell.this.getTreeItem());
                }
                buildModelFromView(DspTreeCell.this.getTreeView().getRoot(), DspTreeCell.this.getTreeView().getRoot().getValue());
                // adjust move time in a DSP, if needed
                DspTreeCell.this.getTreeView().getRoot().getValue().checkTreeItems();
                // trigger listener to update the view
                triggerListener();
            }
        });
        // Dsp Context Menu
        MenuItem cMenuDeleteDsp = new MenuItem("Delete");
        cMenuDsp.getItems().add(cMenuDeleteDsp);
        cMenuDeleteDsp.setOnAction(new EventHandler() {
            public void handle(Event event) {
                // if this is in a non-editable TreeView, don't delete
                if (DspTreeCell.this.getTreeView().isEditable())
                    DspTreeCell.this.getTreeItem().getParent().getChildren().remove(DspTreeCell.this.getTreeItem());
                buildModelFromView(DspTreeCell.this.getTreeView().getRoot(), DspTreeCell.this.getTreeView().getRoot().getValue());
                // adjust move time in a DSP, if needed
                DspTreeCell.this.getTreeView().getRoot().getValue().checkTreeItems();
                // trigger listener to update the view
                triggerListener();
            }
        });
    }

    {
        this.setOnMouseClicked((MouseEvent event) -> {
            // trigger listener to update the view
            triggerListener();
            event.consume();
        });

/*
        this.setOnMouseExited((MouseEvent event) -> {
            DspTreeCell.this.getTreeView().getSelectionModel().clearSelection();
            event.consume();
        });
*/

        this.setOnDragDetected((MouseEvent event) -> {
            ClipboardContent content;
            
            content = new ClipboardContent();
            content.putString("DragItem");
            
            Dragboard dragboard;
            
            dragboard = DspTreeCell.this.getTreeView().startDragAndDrop(TransferMode.MOVE);
            dragboard.setContent(content);
            
            draggedTreeItem = DspTreeCell.this.getTreeItem();
            draggedTreeView = DspTreeCell.this.getTreeView();

            event.consume();
        });

        this.setOnDragEntered((DragEvent event) -> {
             //this.setStyle("-fx-background-color: grey;");
        });

        this.setOnDragExited((DragEvent event) -> {
             this.setStyle("");
             this.setEffect(null);
        });

        this.setOnDragOver((DragEvent event) -> {
          // Only allow drag&drop in non-library treeView
          // Library TreeView has showRoot turned off
          if (DspTreeCell.this.getTreeView().isShowRoot()) { 
            // Target Cell empty?
            if (DspTreeCell.this.isEmpty()) {
              dropType = DropType.DROP_INTO_EMPTY;
              event.acceptTransferModes(TransferMode.MOVE);
            } else {
              // Target Cell cannot be Child of the moving cell
              if (isNotChildOfDraggedCell(DspTreeCell.this.getTreeItem())) {
                Point2D sceneCoordinates = DspTreeCell.this.localToScene(0d, 0d);

                double height = DspTreeCell.this.getHeight();

                // get the y coordinate within the control
                double y = event.getSceneY() - (sceneCoordinates.getY());

                if (DspTreeCell.this.getItem() instanceof DspGrp) {
                  // Target Cell for DROP_INTO can only be DspGrp
                  // set the effect for the required action

                  // if the drop target is up to a quarter of the way down the
                  // control, move above.
                  // if the drop target is more than a quarter of the way down the
                  // control, but less than three quarters, and the item is an
                  // instance of a DspGrp, then drop into
                  // if the drop target is more than three quarters of the way
                  // down the control, then move below.
                  if ((y > height * .25d) & (y < (height * .75d))) {
                    InnerShadow shadow;
                    shadow = new InnerShadow();
                    shadow.setOffsetX(1.0);
                    shadow.setColor(Color.web("#666666"));
                    shadow.setOffsetY(1.0);
                    setEffect(shadow);
                    dropType = DropType.DROP_INTO;
                  } else if (y > (height * .75d)) {
                    setEffect(null);
                    dropType = DropType.MOVE_BELOW;
                  } else if (y < (height * .25d)) {
                    setEffect(null);
                    dropType = DropType.MOVE_ABOVE;
                  }
                } else {
                  if (y > (height * .5d)) {
                    setEffect(null);
                    dropType = DropType.MOVE_BELOW;
                  } else if (y <= (height * .5d)) {
                    setEffect(null);
                    dropType = DropType.MOVE_ABOVE;
                  }
                }
                event.acceptTransferModes(TransferMode.MOVE);
              }
            }
          }
        });

        this.setOnDragDropped((DragEvent event) -> {
          boolean dropOk = false;
          boolean sameOriginTree = false;

          // figure out who draggedTreeItem's parent was, so we can remove it
          TreeItem<DspTreeItem> draggedTreeItemParent = draggedTreeItem.getParent();
          // figure out this cell's parent
          TreeItem<DspTreeItem> targetTreeItemParent;
          if (DspTreeCell.this.isEmpty()) {
            targetTreeItemParent = DspTreeCell.this.getTreeView().getRoot();
          } else {
            targetTreeItemParent = DspTreeCell.this.getTreeItem().getParent();
          }

          // only remove the item from the tree if they are from the same tree, i.e. sameOriginTree = true
          if (draggedTreeView.getRoot().equals(DspTreeCell.this.getTreeView().getRoot())) sameOriginTree = true;

          // standard location in the drop
          int addloc = 0;

          if (draggedTreeItem != null) {
            if ((dropType == DropType.DROP_INTO) & !(DspTreeCell.this.getItem() instanceof Dsp)) {
              // modify TreeView
              // have to add a true copy of the DSP if we copied from the library
              // if not, then a reference is ok
              // check whether we are dropping into an empty cell; in case
              // that we do, we simply attach the item to the end of the
              // root
              if (sameOriginTree) {
                draggedTreeItemParent.getChildren().remove(draggedTreeItem);
                DspTreeCell.this.getTreeItem().getChildren().add(addloc, draggedTreeItem);
              } else {
                DspTreeCell.this.getTreeItem().getChildren().add(addloc, new TreeItem<>(((Dsp)draggedTreeItem.getValue()).copy()));
              }

              // make sure dropped item is not isLib anymore
              if (draggedTreeItem.getValue() instanceof Dsp) {
                ((Dsp)DspTreeCell.this.getTreeItem().getChildren().get(addloc).getValue()).setIsLib(false);
              }
              dropOk = true;
            } else if (dropType == DropType.DROP_INTO_EMPTY) {
              // if target parent is not empty, then add to the end of the sequence
              if (!targetTreeItemParent.getChildren().isEmpty()) {
                addloc = targetTreeItemParent.getChildren().size();
              }
              // modify TreeView
              // have to add a true copy of the DSP if we copied from the library
              // if not, then a reference is ok
              // check whether we are dropping into an empty cell; in case
              // that we do, we simply attach the item to the end of the
              // root
              if (sameOriginTree) {
                targetTreeItemParent.getChildren().add(addloc, draggedTreeItem);
                draggedTreeItemParent.getChildren().remove(draggedTreeItem);
                // need following for the scenario where we moved an item in the
                // same tree to the end of the sequence
                // (it was added as xxx.size() but then the old one was removed
                // thus reducing the size of the list by on)
                if (addloc == targetTreeItemParent.getChildren().size()) addloc -= 1;
              } else {
                targetTreeItemParent.getChildren().add(addloc, new TreeItem<>(((Dsp)draggedTreeItem.getValue()).copy()));
              }

              // make sure dropped item is not isLib anymore
              // because we removed the 'old' item after 
              if (draggedTreeItem.getValue() instanceof Dsp) {
                ((Dsp)targetTreeItemParent.getChildren().get(addloc).getValue()).setIsLib(false);
              }
              dropOk = true;
            } else if (dropType == DropType.MOVE_ABOVE) {
              // we cannot go above the Root node
              // so, if anyone points above Root node, then do nothing
              // have to add a true copy of the DSP if we copied from the library
              // if not, then a reference is ok
              // check whether we are dropping into an empty cell; in case
              // that we do, we simply attach the item to the end of the
              // root
              if (!(DspTreeCell.this.getItem().equals(DspTreeCell.this.getTreeView().getRoot().getValue()))) {
                // change addloc to reflect correct insertion location (ABOVE)
                addloc = targetTreeItemParent.getChildren().indexOf(DspTreeCell.this.getTreeItem());

                if (sameOriginTree) {
                  draggedTreeItemParent.getChildren().remove(draggedTreeItem);
                  targetTreeItemParent.getChildren().add(addloc, draggedTreeItem);
                } else {
                  targetTreeItemParent.getChildren().add(addloc, new TreeItem<>(((Dsp)draggedTreeItem.getValue()).copy()));
                }

                // make sure dropped item is not isLib anymore
                if (draggedTreeItem.getValue() instanceof Dsp) {
                  ((Dsp)targetTreeItemParent.getChildren().get(addloc).getValue()).setIsLib(false);
                }
                dropOk = true;
              }
            } else if (dropType == DropType.MOVE_BELOW) {
              // we cannot go below the Root node
              // so, if anyone points below Root node, then do nothing
              // have to add a true copy of the DSP if we copied from the
              // library if not, then a reference is ok
              // check whether we are dropping into an empty cell; in case
              // that we do, we simply attach the item to the end of the
              // root
              if (!(DspTreeCell.this.getItem().equals(DspTreeCell.this.getTreeView().getRoot().getValue()))) {
                // change addloc to reflect correct insertion location
                addloc = targetTreeItemParent.getChildren().indexOf(DspTreeCell.this.getTreeItem());

                if (sameOriginTree) {
                  // have to reverse removal order because running into issues
                  // otherwise if moving to the end of the sequence
                  draggedTreeItemParent.getChildren().remove(draggedTreeItem);
                  // if added to the end of the sequence, just add and addloc
                  // will have the right index already
                  if (addloc == targetTreeItemParent.getChildren().size()-1) {
                    targetTreeItemParent.getChildren().add(draggedTreeItem);
                  // else add at the location of addloc
                  } else {
                    targetTreeItemParent.getChildren().add(addloc, draggedTreeItem);
                  }
                } else {
                  // if added to the end of the sequence, just add and addloc
                  // will have the right index already
                  if (addloc == targetTreeItemParent.getChildren().size()-1) {
                    targetTreeItemParent.getChildren().add(new TreeItem<>(((Dsp)draggedTreeItem.getValue()).copy()));
                  // else add at the location of addloc
                  } else {
                    targetTreeItemParent.getChildren().add(addloc, new TreeItem<>(((Dsp)draggedTreeItem.getValue()).copy()));
                  }
                }

                // make sure dropped item is not isLib anymore
                if (draggedTreeItem.getValue() instanceof Dsp) {
                  ((Dsp)targetTreeItemParent.getChildren().get(addloc).getValue()).setIsLib(false);
                }
              }
              dropOk = true;
            }
          }

          event.setDropCompleted(dropOk);

          // reset our copy buffers
          draggedTreeItem = null;
          draggedTreeView = null;

          // adjust the Model
          buildModelFromView(DspTreeCell.this.getTreeView().getRoot(), DspTreeCell.this.getTreeView().getRoot().getValue());
          // adjust move time in a DSP, if needed
          DspTreeCell.this.getTreeView().getRoot().getValue().checkTreeItems();

          // trigger listener to update the view
          triggerListener(addloc);

          event.consume();
      });
    }

    protected void triggerListener() {
        // need this to update the details view
        // [need to trigger the Listener]
        DspTreeCell.this.getTreeView().getSelectionModel().clearSelection();
        DspTreeCell.this.getTreeView().getSelectionModel().select(DspTreeCell.this.getTreeItem());           
    }
    
    protected void triggerListener(int dropLocation) {
        // need this to update the details view
        // [need to trigger the Listener]
        DspTreeCell.this.getTreeView().getSelectionModel().clearSelection();
        DspTreeCell.this.getTreeView().getSelectionModel().select(dropLocation);           
    }
    
    protected boolean isNotChildOfDraggedCell(TreeItem<DspTreeItem> treeItemParent) {
      if (treeItemParent != null) {
        if (draggedTreeItem == treeItemParent)
            return false;
        
        if (treeItemParent.getParent() != null)
            return isNotChildOfDraggedCell(treeItemParent.getParent());
        else
            return true;
      } else {
        // if the treeItemParent is null, then we were pointing to an empty cell
        return true;
      }
    }
    
    protected void buildModelFromView(TreeItem<DspTreeItem> topLevel, DspTreeItem topItem) {
        // this is recursive, assumes that 
        // topItem corresponds to a DspGrp, and
        // topLevel corresponds to the level of topItem in the tree

        // clear current
        topItem.getTreeItems().clear();

        for (int i=0; i<topLevel.getChildren().size(); i++) {
            topItem.add(topLevel.getChildren().get(i).getValue());

            buildModelFromView(topLevel.getChildren().get(i), (DspTreeItem)topItem.getTreeItems().get(i));
        }
    }

    /**
     * Context menu functions and callbacks
     */
    private String getStringNumCycles() {
        return String.valueOf(((DspGrp)DspTreeCell.this.getTreeItem().getValue()).getNumCycles());
    }

    // need this for updating NumCycles from the context menu
    private void createTextField() {
        textField = new TextField(getStringNumCycles());
        textField.setPrefColumnCount(3);
        textField.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    ((DspGrp)getTreeItem().getValue()).setNumCycles(Integer.parseInt(textField.getText()));
                    commitEdit(DspTreeCell.this.getItem());
                    buildModelFromView(DspTreeCell.this.getTreeView().getRoot(), DspTreeCell.this.getTreeView().getRoot().getValue());
                    // adjust move time in a DSP, if needed
                    DspTreeCell.this.getTreeView().getRoot().getValue().checkTreeItems();
                    // trigger listener to update the view
                    triggerListener();
                } else if (event.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
                event.consume();
            }
        });
    }

    @Override
    public void startEdit() {
        super.startEdit();

        if (DspTreeCell.this.getTreeItem().getValue() instanceof DspGrp) {
            if (textField == null) {
                createTextField();
            } else {
                textField.setText(getStringNumCycles());
            }
            setText(null);
            setGraphic(textField);
            textField.selectAll();
        }
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        if (DspTreeCell.this.getTreeItem().getValue() instanceof DspGrp) {
            setText(getTreeItem().getValue().getItemName() + " [" + getStringNumCycles() + "]");
            setGraphic(getTreeItem().getGraphic());
        }
    }

    @Override
    public void updateItem(DspTreeItem item, boolean empty) {
        super.updateItem(item, empty);
 
        if (empty) {
            setText(null);
        } else {
            if (item instanceof DspGrp) {
                // shows name plus number of cycles
                setContextMenu(cMenuDspGrp);
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getStringNumCycles());
                    }
                    setGraphic(textField);
                } else {
                    setText(item.getItemName() + " [" + getStringNumCycles() + "]");
                    setGraphic(getTreeItem().getGraphic());
                }
            } else {
                setText(item.getItemName());
                setContextMenu(cMenuDsp);
            }
        }
    }
}
