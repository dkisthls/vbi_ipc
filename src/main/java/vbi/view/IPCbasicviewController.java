/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vbi.view;

import vbi.ipc;
import vbi.model.DspTreeItem;
import vbi.model.DspGrp;
import vbi.model.Dsp;

import java.util.Collections;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.Group;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.util.ArrayList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Toggle;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 *
 * @author fwoeger
 */
public class IPCbasicviewController {
    DspTreeItem displayedItemBlue = null;
    DspTreeItem displayedItemRed = null;
  
    @FXML
    private TreeView<DspTreeItem> dspTreeViewPrefabBlue;
    @FXML
    private TreeView<DspTreeItem> dspTreeViewPrefabRed;

    @FXML
    private TreeView<DspTreeItem> dspTreeViewBlue;
    @FXML
    private TreeView<DspTreeItem> dspTreeViewRed;
    
    @FXML
    private TextField dspNameTextFieldBlue;
    @FXML
    private TextField dspNameTextFieldRed;
    @FXML
    private TextField wavelengthTextFieldBlue;
    @FXML
    private TextField wavelengthTextFieldRed;
    @FXML
    private TextField exptimeTextFieldBlue;
    private String tETBlue;               // temporary helper var
    @FXML
    private TextField exptimeTextFieldRed;
    private String tETRed;                // temporary helper var
    @FXML
    private TextField framerateTextFieldBlue;
    private String tFRBlue;               // temporary helper var
    @FXML
    private TextField framerateTextFieldRed;
    private String tFRRed;                // temporary helper var
    @FXML
    private CheckBox reconstructToggleBlue;
    @FXML
    private CheckBox reconstructToggleRed;
    @FXML
    private CheckBox frameselectToggleBlue;
    @FXML
    private CheckBox frameselectToggleRed;
    @FXML
    private TextField numacqimsTextFieldBlue;
    private String tAIBlue;               // temporary helper var
    @FXML
    private TextField numacqimsTextFieldRed;
    private String tAIRed;                // temporary helper var
    @FXML
    private TextField numselimsTextFieldBlue;
    private String tSIBlue;               // temporary helper var
    @FXML
    private TextField numselimsTextFieldRed;
    private String tSIRed;                // temporary helper var
    @FXML
    private TextField fieldsampleTextFieldBlue;
    @FXML
    private TextField fieldsampleTextFieldRed;
    @FXML
    private TextField roiTextFieldBlue;
    private String tROIBlue;              // temporary helper var
    @FXML
    private TextField roiTextFieldRed;
    private String tROIRed;               // temporary helper var
    @FXML
    private TextField binningTextFieldBlue;
    private String tBINBlue;              // temporary helper var
    @FXML
    private TextField binningTextFieldRed;
    private String tBINRed;               // temporary helper var
    @FXML
    private TextField snrTextFieldBlue;
    @FXML
    private TextField snrTextFieldRed;
    @FXML
    private TextField durationTextFieldBlue;
    @FXML
    private TextField durationTextFieldRed;
    @FXML
    private CheckBox coronalUseToggleBlue;
    @FXML
    private CheckBox coronalUseToggleRed;
    @FXML
    private TextField relCoronalFluxTextFieldBlue;
    private String tRCUFBlue;
    @FXML
    private TextField relCoronalBackgroundTextFieldBlue;
    private String tRCUBBlue;
    @FXML
    private TextField relCoronalFluxTextFieldRed;
    private String tRCUFRed;
    @FXML
    private TextField relCoronalBackgroundTextFieldRed;
    private String tRCUBRed;

    @FXML
    private Group fieldSelectGroupBlue;
    @FXML
    private Group fieldSelectGroupRed;
    private Image sampleImageBlue;
    private Image sampleImageRed;
    private ImageView sampleImageViewBlue;
    private ImageView sampleImageViewRed;
    private ToggleGroup tgFieldSelectBlue;
    private ToggleGroup tgFieldSelectRed;
    private ArrayList alFieldSelectBlue;
    private ArrayList alFieldSelectRed;
    @FXML
    private RadioButton rbAllBlue;
    @FXML
    private RadioButton rbAllRed;
    @FXML
    private RadioButton rbCenterBlue;
    @FXML
    private RadioButton rbCenterRed;
    @FXML
    private RadioButton rbCustomBlue;
    @FXML
    private RadioButton rbCustomRed;

    @FXML
    private LineChart<Number,Number> lineChartBlue;
    @FXML
    private LineChart<Number,Number> lineChartRed;
    @FXML
    private NumberAxis xAxisBlue;
    @FXML
    private NumberAxis xAxisRed;
    @FXML
    private NumberAxis yAxisBlue;
    @FXML
    private NumberAxis yAxisRed;
    
    @FXML
    private Label seqDurationBlue;
    @FXML
    private Label seqDurationRed;
    @FXML
    private Label avgDataRateBlue;
    @FXML
    private Label avgDataRateRed;
    @FXML
    private Label dataVolumeBlue;
    @FXML
    private Label dataVolumeRed;
    @FXML
    private Label peakDataRateBlue;
    @FXML
    private Label peakDataRateRed;
    @FXML
    private Label totalDurationBlue;
    @FXML
    private Label totalDurationRed;
    @FXML
    private Pane pTimeLineBlue;
    @FXML
    private Pane pTimeLineRed;

    // Synchronization Accordeon
    private ToggleGroup tgSyncSelection;
    @FXML
    private RadioButton rbSyncNone;
    @FXML
    private RadioButton rbSyncLoose;
    @FXML
    private RadioButton rbSyncFixed;
    @FXML
    private Pane pSyncTimeLineBlue;
    @FXML
    private Pane pSyncTimeLineRed;
    @FXML
    private Label maxSequenceDuration;
    @FXML
    private Label totalDuration;
    @FXML
    private Label totalDataVolume;
    
    // Log Messages Accordeon
    @FXML
    private TextArea logMessages;

    // reference to the main application
    private ipc appIpc;
    
    /**
     * Setting pixel size here.
     * I am not happy about it either, but I think it has to be this way.
     */
    private static final double PIXELFOVBLUE = Math.pow(430.5e-9 / 4.0 * (360.*60.*60./(2.*Math.PI)) / 2.0, 2.0);
    private static final double PIXELFOVRED  = Math.pow(656.3e-9 / 4.0 * (360.*60.*60./(2.*Math.PI)) / 2.0, 2.0);

    /**
     * Tooltip texts
     */
    private static final String TTTREEVIEW =
        "Drag & drop items from the Data Set Library here.\n"
       +"Right click to create SubCycles.";
    private static final String TTEXPTIME =
        "Adjust exposure time.\n"
       +"[Has to match Frame Rate]";
    private static final String TTFRAMERATE =
        "Expert Mode:\n"
       +"Adjust frame rate.\n"
       +"[Exposure time might have to be adjusted manually]";
    private static final String TTRECON =
        "(De-)activate Speckle Image Reconstruction\n"
       +">= 80 images are required for a reconstruction.\n"
       +"<= 10 raw images allowable if reconstruction deactivated.\n"
       +"[Scientific justification needed if deactivated]";
    private static final String TTFRAMESEL =
        "(De-)activate Frame Selection.";
    private static final String TTSELIMS =
        "Number of images to be selected,\n"
       +"if FrameSelection is activated.\n"
       +"[<= acquired images]";
    private static final String TTACQIMS =
        "Number of images to be acquired.";
    private static final String TTFIELDSMPL =
        "Number of field samples currently selected.\n"
       +"Use the Field Selection pane to modify.";
    private static final String TTROI =
        "Expert Mode:\n"
       +"Adjust Region Of Interest in full frame space (LLX, LLY, DX, DY)\n"
       +"[single field], [no speckle reconstruction]";
    private static final String TTBIN =
        "Expert Mode:\n"
       +"Adjust the Binning Factor (1-4).";
    private static final String TTSNR =
        "Expected Signal To Noise ratio in individual images.\n"
       +"Photon Noise is assumed to be dominant.";
    private static final String TTDUR =
        "Expected duration of this item.";
    private static final String TTCORONALCB =
        "Expert Mode:\n"
       +"If this wavelength is intended to be used \n"
       +"for coronal purposes, please activate.";
    private static final String TTCORONALTF =
        "Expert Mode:\n"
       +"Enter, in MILLIONTH relative to disk center flux, \n"
       +"the expected flux of the coronal signal.";
    private static final String TTCORONALBGTF =
        "Expert Mode:\n"
       +"Enter, in MILLIONTH relative to disk center flux, \n"
       +"the expected BACKGROUND flux in coronal observations.";
    private static final String TTPREFAB =
        "Drag and drop items from here to the Sequence tree.\n"
       +"Click on the items in the Sequence tree to modify properties.";

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public IPCbasicviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // >>>>> BLUE
        // Initialize the DSP TableView Blue to be editable
        dspTreeViewBlue.setEditable(true);
        // Initialize the TreeView to only allow one selection
        dspTreeViewBlue.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        // display the itemName in TreeView, not something weird
        // set up Drag and Drop
        dspTreeViewBlue.setCellFactory(cellData -> new DspTreeCell());
        // Listen for selection changes and show the person details when changed.
        dspTreeViewBlue.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> {
                if (newValue != null) {
                    displayedItemBlue = newValue.getValue();
                    dspTreeViewBlue.getRoot().getValue().checkTreeItems();
                    showDspDetailsBlue();
                    showSummaryInfoBlue();
                }
            }
        );
        Tooltip.install(dspTreeViewBlue, new Tooltip(TTTREEVIEW));
        // >>>>> RED
        // Initialize the DSP TableView Red to be editable
        dspTreeViewRed.setEditable(true);
        // Initialize the TreeView to only allow one selection
        dspTreeViewRed.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        // display the itemName in TreeView, not something weird
        // set up Drag and Drop
        dspTreeViewRed.setCellFactory(cellData -> new DspTreeCell());
        // Listen for selection changes and show the person details when changed.
        dspTreeViewRed.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> {
                if (newValue != null) {
                    displayedItemRed = newValue.getValue();
                    dspTreeViewRed.getRoot().getValue().checkTreeItems();
                    showDspDetailsRed();
                    showSummaryInfoRed();
                }
            }
        );
        Tooltip.install(dspTreeViewRed, new Tooltip(TTTREEVIEW));
 
        /** GENERAL comment:
         *  The Focus listeners help in two ways:
         *  - they only focus the item in the sequence tree if something changed
         *    which is perfect because that is the only way a DSP can change
         *    (because it is not isLib) ...
         *  - ... by calling a check function when the focus leaves which
         *    checks the input values
         *  - it feels a bit hackish, but I think it is a pretty clever way to
         *    update stuff when the cursor moves out of the field without
         *    causing other issues (checkXXX() would eg focus a Sequence
         *    DSP when a Library item had been focused
         */
        // >>>>> BLUE
        // set DSP TextFields Blue default editability
        dspNameTextFieldBlue.setEditable(false);
        wavelengthTextFieldBlue.setEditable(false);
        exptimeTextFieldBlue.setEditable(false);
        exptimeTextFieldBlue.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tETBlue = exptimeTextFieldBlue.getText();
            }
            if (oldValue) {
                if (!(exptimeTextFieldBlue.getText().compareTo(tETBlue) == 0)) {
                    checkExpTimeInputBlue();
                    tETBlue = exptimeTextFieldBlue.getText();
                }
            }
        });
        Tooltip.install(exptimeTextFieldBlue, new Tooltip(TTEXPTIME));
        framerateTextFieldBlue.setEditable(false);
        framerateTextFieldBlue.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tFRBlue = framerateTextFieldBlue.getText();
            }
            if (oldValue) {
                if (!(framerateTextFieldBlue.getText().compareTo(tFRBlue) == 0)) {
                    checkFrameRateInputBlue();
                    tFRBlue = framerateTextFieldBlue.getText();
                }
            }
        });
        Tooltip.install(framerateTextFieldBlue, new Tooltip(TTFRAMERATE));
        reconstructToggleBlue.setDisable(true);
        reconstructToggleBlue.setAllowIndeterminate(false);
        reconstructToggleBlue.selectedProperty().addListener((observable, oldValue, newValue) -> {
          if (appIpc.getExpertMode()) {
            if (!newValue) {
              roiTextFieldBlue.setEditable(true);
              binningTextFieldBlue.setEditable(true);
            } else {
              roiTextFieldBlue.setEditable(false);
              binningTextFieldBlue.setEditable(false);
            }
          }
        });
        Tooltip.install(reconstructToggleBlue, new Tooltip(TTRECON));
        frameselectToggleBlue.setDisable(true);
        frameselectToggleBlue.setAllowIndeterminate(false);
        frameselectToggleBlue.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                numselimsTextFieldBlue.setEditable(true);
            } else {
                numselimsTextFieldBlue.setEditable(false);
            }
        });
        Tooltip.install(frameselectToggleBlue, new Tooltip(TTFRAMESEL));
        numselimsTextFieldBlue.setEditable(false);
        numselimsTextFieldBlue.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tSIBlue = numselimsTextFieldBlue.getText();
            }
            if (oldValue) {
                if (!(numselimsTextFieldBlue.getText().compareTo(tSIBlue) == 0)) {
                    checkNumFramesInputBlue();
                    tSIBlue = numselimsTextFieldBlue.getText();
                }
            }
        });
        Tooltip.install(numselimsTextFieldBlue, new Tooltip(TTSELIMS));
        numacqimsTextFieldBlue.setEditable(false);
        numacqimsTextFieldBlue.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tAIBlue = numacqimsTextFieldBlue.getText();
            }
            if (oldValue) {
                if (!(numacqimsTextFieldBlue.getText().compareTo(tAIBlue) == 0)) {
                    checkNumFramesInputBlue();
                    tAIBlue = numacqimsTextFieldBlue.getText();
                }
            }
        });
        Tooltip.install(numacqimsTextFieldBlue, new Tooltip(TTACQIMS));
        fieldsampleTextFieldBlue.setEditable(false);
        Tooltip.install(fieldsampleTextFieldBlue, new Tooltip (TTFIELDSMPL));
        roiTextFieldBlue.setEditable(false);
        roiTextFieldBlue.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tROIBlue = roiTextFieldBlue.getText();
            }
            if (oldValue) {
                if (!(roiTextFieldBlue.getText().compareTo(tROIBlue) == 0)) {
                    checkROIInputBlue();
                    tROIBlue = roiTextFieldBlue.getText();
                }
            }
        });
        Tooltip.install(roiTextFieldBlue, new Tooltip(TTROI));
        binningTextFieldBlue.setEditable(false);
        binningTextFieldBlue.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tBINBlue = binningTextFieldBlue.getText();
            }
            if (oldValue) {
                if (!(binningTextFieldBlue.getText().compareTo(tBINBlue) == 0)) {
                    checkBinInputBlue();
                    tBINBlue = binningTextFieldBlue.getText();
                }
            }
        });
        Tooltip.install(binningTextFieldBlue, new Tooltip(TTBIN));
        snrTextFieldBlue.setEditable(false);
        Tooltip.install(snrTextFieldBlue, new Tooltip(TTSNR));
        durationTextFieldBlue.setEditable(false);
        Tooltip.install(durationTextFieldBlue, new Tooltip(TTDUR));
        coronalUseToggleBlue.setSelected(false);
        coronalUseToggleBlue.setDisable(true);
        coronalUseToggleBlue.selectedProperty().addListener((observable, oldValue, newValue) -> {
          if (appIpc.getExpertMode()) {
            if (newValue) {
              relCoronalFluxTextFieldBlue.setEditable(true);
              relCoronalBackgroundTextFieldBlue.setEditable(true);
            } else {
              relCoronalFluxTextFieldBlue.setEditable(false);
              relCoronalFluxTextFieldBlue.clear();
              relCoronalBackgroundTextFieldBlue.setEditable(false);
              relCoronalBackgroundTextFieldBlue.clear();
              checkRelCoronalFluxInputBlue();
              checkExpTimeInputBlue();
            }
          }
        });
        Tooltip.install(coronalUseToggleBlue, new Tooltip(TTCORONALCB));
        relCoronalFluxTextFieldBlue.setEditable(false);
        relCoronalFluxTextFieldBlue.focusedProperty().addListener((observable, oldValue, newValue) -> {
          // just focused the field
          if (newValue) {
            tRCUFBlue = relCoronalFluxTextFieldBlue.getText();
          }
          if (oldValue) {
            if (!(relCoronalFluxTextFieldBlue.getText().compareTo(tRCUFBlue) == 0)) {
              checkRelCoronalFluxInputBlue();
              tRCUFBlue = relCoronalFluxTextFieldBlue.getText();
            }
          }
        });
        Tooltip.install(relCoronalFluxTextFieldBlue, new Tooltip(TTCORONALTF));
        relCoronalBackgroundTextFieldBlue.setEditable(false);
        relCoronalBackgroundTextFieldBlue.focusedProperty().addListener((observable, oldValue, newValue) -> {
          // just focused the field
          if (newValue) {
            tRCUBBlue = relCoronalBackgroundTextFieldBlue.getText();
          }
          if (oldValue) {
            if (!(relCoronalBackgroundTextFieldBlue.getText().compareTo(tRCUBBlue) == 0)) {
              checkRelCoronalBackgroundInputBlue();
              tRCUBBlue = relCoronalBackgroundTextFieldBlue.getText();
            }
          }
        });
        Tooltip.install(relCoronalBackgroundTextFieldBlue, new Tooltip(TTCORONALBGTF));
        // clear DSP TextField details
        showDspDetailsBlue();

        // >>>>> RED
        // set DSP TextFields Blue default editability
        dspNameTextFieldRed.setEditable(false);
        wavelengthTextFieldRed.setEditable(false);
        exptimeTextFieldRed.setEditable(false);
        exptimeTextFieldRed.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tETRed = exptimeTextFieldRed.getText();
            }
            if (oldValue) {
                if (!(exptimeTextFieldRed.getText().compareTo(tETRed) == 0)) {
                    checkExpTimeInputRed();
                    tETRed = exptimeTextFieldRed.getText();
                }
            }
        });
        Tooltip.install(exptimeTextFieldRed, new Tooltip(TTEXPTIME));
        framerateTextFieldRed.setEditable(false);
        framerateTextFieldRed.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tFRRed = framerateTextFieldRed.getText();
            }
            if (oldValue) {
                if (!(framerateTextFieldRed.getText().compareTo(tFRRed) == 0)) {
                    checkFrameRateInputRed();
                    tFRRed = framerateTextFieldRed.getText();
                }
            }
        });
        Tooltip.install(framerateTextFieldRed, new Tooltip(TTFRAMERATE));
        reconstructToggleRed.setDisable(true);
        reconstructToggleRed.setAllowIndeterminate(false);
        reconstructToggleRed.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (appIpc.getExpertMode()) {
                if (!newValue) {
                    roiTextFieldRed.setEditable(true);
                    binningTextFieldRed.setEditable(true);
                } else {
                    roiTextFieldRed.setEditable(false);
                    binningTextFieldRed.setEditable(false);
                }
            }
        });
        Tooltip.install(reconstructToggleRed, new Tooltip(TTRECON));
        frameselectToggleRed.setDisable(true);
        frameselectToggleRed.setAllowIndeterminate(false);
        frameselectToggleRed.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue) {
                    numselimsTextFieldRed.setEditable(true);
                } else {
                    numselimsTextFieldRed.setEditable(false);
                }
        });
        Tooltip.install(frameselectToggleRed, new Tooltip(TTFRAMESEL));
        numselimsTextFieldRed.setEditable(false);
        numselimsTextFieldRed.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tSIRed = numselimsTextFieldRed.getText();
            }
            if (oldValue) {
                if (!(numselimsTextFieldRed.getText().compareTo(tSIRed) == 0)) {
                    checkNumFramesInputRed();
                    tSIRed = numselimsTextFieldRed.getText();
                }
            }
        });
        Tooltip.install(numselimsTextFieldRed, new Tooltip(TTSELIMS));
        numacqimsTextFieldRed.setEditable(false);
        numacqimsTextFieldRed.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tAIRed = numacqimsTextFieldRed.getText();
            }
            if (oldValue) {
                if (!(numacqimsTextFieldRed.getText().compareTo(tAIRed) == 0)) {
                    checkNumFramesInputRed();
                    tAIRed = numacqimsTextFieldRed.getText();
                }
            }
        });
        Tooltip.install(numacqimsTextFieldRed, new Tooltip(TTACQIMS));
        fieldsampleTextFieldRed.setEditable(false);
        Tooltip.install(fieldsampleTextFieldRed, new Tooltip(TTFIELDSMPL));
        roiTextFieldRed.setEditable(false);
        roiTextFieldRed.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tROIRed = roiTextFieldRed.getText();
            }
            if (oldValue) {
                if (!(roiTextFieldRed.getText().compareTo(tROIRed) == 0)) {
                    checkROIInputRed();
                    tROIRed = roiTextFieldRed.getText();
                }
            }
        });
        Tooltip.install(roiTextFieldRed, new Tooltip(TTROI));
        binningTextFieldRed.setEditable(false);
        binningTextFieldRed.focusedProperty().addListener((observable, oldValue, newValue) -> {
            // just focused the field
            if (newValue) {
                tBINRed = binningTextFieldRed.getText();
            }
            if (oldValue) {
                if (!(binningTextFieldRed.getText().compareTo(tBINRed) == 0)) {
                    checkBinInputRed();
                    tBINRed = binningTextFieldRed.getText();
                }
            }
        });
        Tooltip.install(binningTextFieldRed, new Tooltip(TTBIN));
        snrTextFieldRed.setEditable(false);
        Tooltip.install(snrTextFieldRed, new Tooltip(TTSNR));
        durationTextFieldRed.setEditable(false);
        Tooltip.install(durationTextFieldRed, new Tooltip(TTDUR));
        coronalUseToggleRed.setSelected(false);
        coronalUseToggleRed.setDisable(true);
        coronalUseToggleRed.selectedProperty().addListener((observable, oldValue, newValue) -> {
          if (appIpc.getExpertMode()) {
            if (newValue) {
              relCoronalFluxTextFieldRed.setEditable(true);
              relCoronalBackgroundTextFieldRed.setEditable(true);
            } else {
              relCoronalFluxTextFieldRed.setEditable(false);
              relCoronalFluxTextFieldRed.clear();
              relCoronalBackgroundTextFieldRed.setEditable(false);
              relCoronalBackgroundTextFieldRed.clear();
              checkRelCoronalFluxInputRed();
              checkExpTimeInputRed();
            }
          }
        });
        Tooltip.install(coronalUseToggleRed, new Tooltip(TTCORONALCB));
        relCoronalFluxTextFieldRed.setEditable(false);
        relCoronalFluxTextFieldRed.focusedProperty().addListener((observable, oldValue, newValue) -> {
          // just focused the field
          if (newValue) {
            tRCUFRed = relCoronalFluxTextFieldRed.getText();
          }
          if (oldValue) {
            if (!(relCoronalFluxTextFieldRed.getText().compareTo(tRCUFRed) == 0)) {
              checkRelCoronalFluxInputRed();
              tRCUFRed = relCoronalFluxTextFieldRed.getText();
            }
          }
        });
        Tooltip.install(relCoronalFluxTextFieldRed, new Tooltip(TTCORONALTF));
        relCoronalBackgroundTextFieldRed.setEditable(false);
        relCoronalBackgroundTextFieldRed.focusedProperty().addListener((observable, oldValue, newValue) -> {
          // just focused the field
          if (newValue) {
            tRCUBRed = relCoronalBackgroundTextFieldRed.getText();
          }
          if (oldValue) {
            if (!(relCoronalBackgroundTextFieldRed.getText().compareTo(tRCUBRed) == 0)) {
              checkRelCoronalBackgroundInputRed();
              tRCUBRed = relCoronalBackgroundTextFieldRed.getText();
            }
          }
        });
        Tooltip.install(relCoronalBackgroundTextFieldRed, new Tooltip(TTCORONALBGTF));
        // clear DSP TextField details
        showDspDetailsRed();

        // >>>>> BLUE
        // Initialize the DSP Library TreeView Blue to not be editable
        dspTreeViewPrefabBlue.setEditable(false);
        // Initialize show Root to false
        dspTreeViewPrefabBlue.setShowRoot(false);
        // Initialize the DSP TableView to only allow one selection
        dspTreeViewPrefabBlue.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        // display the DSP name in TableView, not something weird
        dspTreeViewPrefabBlue.setCellFactory(cellData -> new DspTreeCell());
        // Listen for selection changes and show the person details when changed.
        dspTreeViewPrefabBlue.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> {
                if (newValue != null) {
                    displayedItemBlue = newValue.getValue();
                    showDspDetailsBlue();
                }
            }
        );
        Tooltip.install(dspTreeViewPrefabBlue, new Tooltip(TTPREFAB));
        // >>>>> RED
        // Initialize the DSP Library TreeView Blue to not be editable
        dspTreeViewPrefabRed.setEditable(false);
        // Initialize show Root to false
        dspTreeViewPrefabRed.setShowRoot(false);
        // Initialize the DSP TableView to only allow one selection
        dspTreeViewPrefabRed.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        // display the DSP name in TableView, not something weird
        dspTreeViewPrefabRed.setCellFactory(cellData -> new DspTreeCell());
        // Listen for selection changes and show the person details when changed.
        dspTreeViewPrefabRed.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> {
                if (newValue != null) {
                    displayedItemRed = newValue.getValue();
                    showDspDetailsRed();
                }
            }
        );
        Tooltip.install(dspTreeViewPrefabRed, new Tooltip(TTPREFAB));

        // >>>>> BLUE
        // keeps track of curently selected fields (9 needed)
        alFieldSelectBlue = new ArrayList<>();
        for (int i=0; i<Dsp.NUMFIELDS; i++) alFieldSelectBlue.add(i, true);
        // for the Field Selection:
        //   create a ToggleGroup, 
        //   add Image, RadioButtons
        //   init fieldSelectGroup with proper mouse interaction and Listener
        tgFieldSelectBlue = new ToggleGroup();
        sampleImageBlue = new Image(this.getClass().getResourceAsStream("656.jpg"));
        sampleImageViewBlue = new ImageView(sampleImageBlue);
        sampleImageViewBlue.setFitWidth(200);
        sampleImageViewBlue.setFitHeight(200);
        // make fieldSelectGroup Group interactive
        fieldSelectGroupBlue.setOnMouseClicked((MouseEvent event) -> {
            if (appIpc.getExpertMode()) {
                double w = sampleImageViewBlue.getFitWidth();
                double h = sampleImageViewBlue.getFitHeight();
                double x = event.getX();
                double y = event.getY();
                int indx;
                int indy;
                // horizontal direction
                if (x < w/3) indx = 0; else if (x > 2*w/3) indx = 2; else indx = 1;
                // vertical direction
                if (y < h/3) indy = 0; else if (y > 2*h/3) indy = 2; else indy = 1;
                // toggle
                if (alFieldSelectBlue.get(indy*3+indx).equals(true))
                    alFieldSelectBlue.set(indy*3+indx, false);
                else 
                    alFieldSelectBlue.set(indy*3+indx, true);
                // the following is needed to trigger the ToggleGroup listener
                rbCustomBlue.setSelected(false);
                tgFieldSelectBlue.selectToggle(rbCustomBlue);
            }
        });
        fieldSelectGroupBlue.setBlendMode(BlendMode.MULTIPLY);
        tgFieldSelectBlue.selectedToggleProperty().addListener(
            (observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (newValue.equals(rbCenterBlue)) {
                    // set ArrayList to center field only
                    for (int i=0; i<Dsp.NUMFIELDS; i++) {
                        alFieldSelectBlue.set(i, false);
                    }
                    alFieldSelectBlue.set(4, true);
                } else if (newValue.equals(rbAllBlue)) {
                    // set ArrayList to all
                    for (int i=0; i<Dsp.NUMFIELDS; i++) {
                        alFieldSelectBlue.set(i, true);
                    }
                }
                displayFieldSampleOverlay(null, fieldSelectGroupBlue, sampleImageViewBlue, alFieldSelectBlue, false);
                
                handleFieldSelectBlue();
            }
        });
        rbAllBlue.setToggleGroup(tgFieldSelectBlue);
        rbCenterBlue.setToggleGroup(tgFieldSelectBlue);
        rbCustomBlue.setToggleGroup(tgFieldSelectBlue);
        rbCustomBlue.setDisable(true);
        // >>>>> RED
        // keeps track of curently selected fields (9 needed)
        alFieldSelectRed = new ArrayList<>();
        for (int i=0; i<Dsp.NUMFIELDS; i++) alFieldSelectRed.add(i, true);
        // for the Field Selection:
        //   create a ToggleGroup, 
        //   add Image, RadioButtons
        //   init fieldSelectGroup with proper mouse interaction and Listener
        tgFieldSelectRed = new ToggleGroup();
        sampleImageRed = new Image(this.getClass().getResourceAsStream("656.jpg"));
        sampleImageViewRed = new ImageView(sampleImageRed);
        sampleImageViewRed.setFitWidth(200);
        sampleImageViewRed.setFitHeight(200);
        // make fieldSelectGroup Group interactive
        fieldSelectGroupRed.setOnMouseClicked((MouseEvent event) -> {
            if (appIpc.getExpertMode()) {
                // clear for RED
                for (int i=4; i<Dsp.NUMFIELDS; i++) alFieldSelectRed.set(i, false);
                // the rest the same as blue (with some index variation)
                double w = sampleImageViewRed.getFitWidth();
                double h = sampleImageViewRed.getFitHeight();
                double x = event.getX();
                double y = event.getY();
                int indx;
                int indy;
                // horizontal direction
                if (x < w/2) indx = 0; else indx = 1;
                // vertical direction
                if (y < h/2) indy = 0; else indy = 1;
                // toggle
                if (alFieldSelectRed.get(indy*2+indx).equals(true))
                    alFieldSelectRed.set(indy*2+indx, false);
                else 
                    alFieldSelectRed.set(indy*2+indx, true);
                // the following is needed to trigger the ToggleGroup listener
                rbCustomRed.setSelected(false);
                tgFieldSelectRed.selectToggle(rbCustomRed);
            }
        });
        fieldSelectGroupRed.setBlendMode(BlendMode.MULTIPLY);
        tgFieldSelectRed.selectedToggleProperty().addListener(
            (observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (newValue.equals(rbCenterRed)) {
                    // set ArrayList to center field only
                    for (int i=0; i<Dsp.NUMFIELDS; i++) {
                        alFieldSelectRed.set(i, false);
                    }
                    // (RED Version: index 4 represents 'center field')
                    // somewhat of a hack, because by chance the 3x3 also has
                    // 4 as its center field
                    alFieldSelectRed.set(4, true);
                } else if (newValue.equals(rbAllRed)) {
                    // set ArrayList to all (RED Version)
                    for (int i=0; i<Dsp.NUMFIELDS; i++) {
                        if (i<4)
                            alFieldSelectRed.set(i, true);
                        else
                            alFieldSelectRed.set(i, false);
                    }
                } 
                displayFieldSampleOverlay(null, fieldSelectGroupRed, sampleImageViewRed, alFieldSelectRed, true);
                
                handleFieldSelectRed();
            }
        });
        rbAllRed.setToggleGroup(tgFieldSelectRed);
        rbCenterRed.setToggleGroup(tgFieldSelectRed);
        rbCustomRed.setToggleGroup(tgFieldSelectRed);
        rbCustomRed.setDisable(true);

        // >>>>> BLUE
        // Initialize LineChart
        lineChartBlue.setLegendVisible(false);
        lineChartBlue.setCreateSymbols(false);
        xAxisBlue.setLabel("wavelength [nm]");
        xAxisBlue.setForceZeroInRange(false);
        yAxisBlue.setLabel("Sp.Irrad.[W/m3]");
        // >>>>> RED
        // Initialize LineChart
        lineChartRed.setLegendVisible(false);
        lineChartRed.setCreateSymbols(false);
        xAxisRed.setLabel("wavelength [nm]");
        xAxisRed.setForceZeroInRange(false);
        yAxisRed.setLabel("Sp.Irrad.[W/m3]");
        
        // Synchronization Accordeon
        tgSyncSelection = new ToggleGroup();
        rbSyncNone.setToggleGroup(tgSyncSelection);
        rbSyncLoose.setToggleGroup(tgSyncSelection);
        rbSyncFixed.setToggleGroup(tgSyncSelection);
        tgSyncSelection.selectToggle(rbSyncNone);
        tgSyncSelection.selectedToggleProperty().addListener(
            (observable, oldValue, newValue) -> {
            if (newValue != null) {
                synchronizeAndUpdateDspTreeItems(newValue);
            }
        });
        
        // add some clever text to the log messages
        logMessages.setEditable(false);
        logMessages.appendText("--- VBI IP was initialized ---\n");
    }

    public void postInit() {
        // figure out the Expert Mode
        appIpc.getExpertModeProperty().addListener(
            (observable, oldValue, newValue) -> {
                if (newValue != null) {
                    if (newValue) {
                        try {
                            if (displayedItemBlue instanceof Dsp) {
                                framerateTextFieldBlue.setEditable(true);
                                if (!((Dsp)displayedItemBlue).getSpeckleProcessing()) roiTextFieldBlue.setEditable(true);
                                if (!((Dsp)displayedItemBlue).getSpeckleProcessing()) binningTextFieldBlue.setEditable(true);
                                if (((Dsp)displayedItemBlue).getIsCoronalWavelength()) coronalUseToggleBlue.setDisable(false);
                            }
                        } catch (NullPointerException e) {
                        }
                        rbCustomBlue.setDisable(false);
                        try {
                            if (displayedItemRed instanceof Dsp) {
                                framerateTextFieldRed.setEditable(true);
                                if (!((Dsp)displayedItemRed).getSpeckleProcessing()) roiTextFieldRed.setEditable(true);
                                if (!((Dsp)displayedItemRed).getSpeckleProcessing()) binningTextFieldRed.setEditable(true);
                                if (((Dsp)displayedItemRed).getIsCoronalWavelength()) coronalUseToggleRed.setDisable(false);
                            }
                        } catch (NullPointerException e) {
                        }
                        rbCustomRed.setDisable(false);
                        logMessages.appendText("+++ Entered Expert Mode\n");
                    } else {
                        framerateTextFieldBlue.setEditable(false);
                        roiTextFieldBlue.setEditable(false);
                        binningTextFieldBlue.setEditable(false);
                        rbCustomBlue.setDisable(true);
                        framerateTextFieldRed.setEditable(false);
                        roiTextFieldRed.setEditable(false);
                        binningTextFieldRed.setEditable(false);
                        rbCustomRed.setDisable(true);
                        
                        // clear trees because we just switched back from Expert
                        // mode - I don't know what else to do
                        dspTreeViewBlue.getRoot().getChildren().clear();
                        appIpc.getTRootBlue().getTreeItems().clear();
                        dspTreeViewRed.getRoot().getChildren().clear();
                        appIpc.getTRootRed().getTreeItems().clear();
                        // try to update the views
                        showDspDetailsBlue();
                        showSummaryInfoBlue();
                        showDspDetailsBlue();
                        showSummaryInfoRed();
                        logMessages.appendText("+++ Exited Expert Mode: all previous progress lost as parameter integrity cannot be ensured\n");
                    }
                }
            }
        );
         // adjust fields in case centerDistance is changed
        appIpc.getMuProperty().addListener(
            (observable, oldValue, newValue) -> {
                if (newValue != null) {
                    showDspDetailsBlue();
                    showDspDetailsRed();
                }
                logMessages.appendText("+++ Distance from solar disk center set to mu = "+appIpc.getMu()+"\n");
            }
        );
        // adjust settings after load
        appIpc.getLoadUpdateFlagProperty().addListener(
            (observable, oldValue, newValue) -> {
                // >>>>> BLUE tree
                // Add items to the TreeView
                dspTreeViewBlue.getRoot().getChildren().clear();
                buildViewFromModel(appIpc.getTRootBlue(), dspTreeViewBlue.getRoot());
                showSummaryInfoBlue();
                // >>>>> RED tree
                // Add items to the TreeView
                dspTreeViewRed.getRoot().getChildren().clear();
                buildViewFromModel(appIpc.getTRootRed(), dspTreeViewRed.getRoot());
                showSummaryInfoRed();
            }
        );
        appIpc.getSyncSelProperty().addListener(
            (observable, oldValue, newValue) -> {
                int newval = newValue.intValue();
                // adjust the synchronization selection
                switch (newval) {
                  case 1:
                    tgSyncSelection.selectToggle(rbSyncLoose);
                    break;
                  case 2:
                    tgSyncSelection.selectToggle(rbSyncFixed);
                    break;
                  default:
                    tgSyncSelection.selectToggle(rbSyncNone);
                    break;
                }
                
            }
        );
    }

    /**
     * Fills all text fields to show details about the DSP.
     * If the specified DSP is null, all text fields are cleared.
     * 
     * @param item the dsp or null
     */
    private void showDspDetailsBlue() {
        Dsp dsp = null;

        if (displayedItemBlue instanceof Dsp) {
            dsp = (Dsp)displayedItemBlue;
        }

        if (dsp != null) {
            // set certain text fields editable etc. when necessary.
            if (dsp.getIsLib() == true) {
                framerateTextFieldBlue.setEditable(false);
                exptimeTextFieldBlue.setEditable(false);
                roiTextFieldBlue.setEditable(false);
                binningTextFieldBlue.setEditable(false);
                reconstructToggleBlue.setDisable(true);
                frameselectToggleBlue.setDisable(true);
                numacqimsTextFieldBlue.setEditable(false);
                numselimsTextFieldBlue.setEditable(false);
                coronalUseToggleBlue.setDisable(true);
                relCoronalFluxTextFieldBlue.setEditable(false);
                relCoronalBackgroundTextFieldBlue.setEditable(false);
            } else {
                if (appIpc.getExpertMode()) {
                  framerateTextFieldBlue.setEditable(true);
                  if (!(dsp.getSpeckleProcessing())) {
                    roiTextFieldBlue.setEditable(true);
                    binningTextFieldBlue.setEditable(true);
                  }
                  if (dsp.getIsCoronalWavelength()) {
                    coronalUseToggleBlue.setDisable(false);
                  } else {
                    coronalUseToggleBlue.setDisable(true);
                  }
                }
                exptimeTextFieldBlue.setEditable(true);
                reconstructToggleBlue.setDisable(false);
                frameselectToggleBlue.setDisable(false);
                numacqimsTextFieldBlue.setEditable(true);
            }
            if (dsp.getNumActiveFieldSamples() == Dsp.NUMFIELDS) {
                tgFieldSelectBlue.selectToggle(rbAllBlue);
            } else if ((dsp.getNumActiveFieldSamples() == 1) && 
                       (dsp.getFieldSamples(4).get())) {
                tgFieldSelectBlue.selectToggle(rbCenterBlue);
            } else {
                // have to set alFieldSelect properly
                for (int i=0; i<Dsp.NUMFIELDS; i++) {
                    alFieldSelectBlue.set(i, dsp.getFieldSamples(i).get());
                }
                tgFieldSelectBlue.selectToggle(rbCustomBlue);
            }
            // disable ROI/bin input fields if in expert mode and more than
            // one field sample is selected, otherwise enable
            if (dsp.getNumActiveFieldSamples() == 1) {
                if (appIpc.getExpertMode() && !(dsp.getSpeckleProcessing())) {
                    roiTextFieldBlue.setEditable(true);
                    binningTextFieldBlue.setEditable(true);
                }
            } else {
                if (appIpc.getExpertMode() && !(dsp.getSpeckleProcessing())) {
                    roiTextFieldBlue.setEditable(false);
                    binningTextFieldBlue.setEditable(false);
                }
            }

            // Fill the labels with info from the Dsp object.
            dspNameTextFieldBlue.setText(dsp.getItemName());
            wavelengthTextFieldBlue.setText(Float.toString(dsp.getWaveLength()));
            exptimeTextFieldBlue.setText(Float.toString(dsp.getExpTime()));
            reconstructToggleBlue.setSelected(dsp.getSpeckleProcessing());
            frameselectToggleBlue.setSelected(dsp.getFrameSelection());
            numacqimsTextFieldBlue.setText(Integer.toString(dsp.getNumAcqImages()));
            numselimsTextFieldBlue.setText(Integer.toString(dsp.getNumSelImages()));
            framerateTextFieldBlue.setText(Float.toString(dsp.getFrameRate()));
            fieldsampleTextFieldBlue.setText(Integer.toString(dsp.getNumActiveFieldSamples()));
            roiTextFieldBlue.setText(convertROIInfoToText(dsp));
            binningTextFieldBlue.setText(Integer.toString(dsp.getBinFactor()));
            double photons;
            double noise;
            if (dsp.getRelCoronalFlux() > 0.0) {
              photons = dsp.getPhotonsPerSecond((float)(PIXELFOVBLUE*Math.pow(dsp.getBinFactor(), 2)), 0, true) *
                        dsp.getExpTime()*1e-3  * dsp.getRelCoronalFlux()*1e-6;
              noise = Math.sqrt(dsp.getPhotonsPerSecond((float)(PIXELFOVBLUE*Math.pow(dsp.getBinFactor(), 2)), 0, false) *
                      dsp.getExpTime()*1e-3  * Math.abs(dsp.getRelCoronalBackground())*1e-6 + photons + 
                      Math.pow(Dsp.CAMREADNOISE*dsp.getBinFactor(), 2));
              snrTextFieldBlue.setText(Integer.toString((int)roundToDecimal((float)(photons/noise),0)));
            } else {
              photons = dsp.getPhotonsPerSecond((float)(PIXELFOVBLUE*Math.pow(dsp.getBinFactor(), 2)),
                        appIpc.getMu(), false) * dsp.getExpTime()*1e-3;
              snrTextFieldBlue.setText(Integer.toString((int)roundToDecimal((float)Math.sqrt(photons),0)));
            }
            durationTextFieldBlue.setText(Float.toString(roundToDecimal(dsp.getRawDuration(), 3)));
            if (!dsp.getIsLib() & dsp.getIsCoronalWavelength()) {
              if (dsp.getRelCoronalFlux() > 0.0) {
                relCoronalFluxTextFieldBlue.setText(Float.toString(dsp.getRelCoronalFlux()));
                relCoronalFluxTextFieldBlue.setEditable(true);
                relCoronalBackgroundTextFieldBlue.setText(Float.toString(dsp.getRelCoronalBackground()));
                relCoronalBackgroundTextFieldBlue.setEditable(true);
                coronalUseToggleBlue.setSelected(true);
              }
            } else {
              relCoronalFluxTextFieldBlue.clear();
              relCoronalFluxTextFieldBlue.setEditable(false);
              relCoronalBackgroundTextFieldBlue.clear();
              relCoronalBackgroundTextFieldBlue.setEditable(false);
              coronalUseToggleBlue.setSelected(false);
            }
            // display field sample overlay
            displayFieldSampleOverlay(dsp, fieldSelectGroupBlue, sampleImageViewBlue, alFieldSelectBlue, false);

            // update line graph
            lineChartBlue.getData().clear();
            lineChartBlue.getData().add(setLineChartData(dsp, "spectrum"));
            lineChartBlue.getData().add(setLineChartData(dsp, "profile"));
        } else {
            // Dsp is null, remove all the text.
            dspNameTextFieldBlue.clear();
            wavelengthTextFieldBlue.clear();
            exptimeTextFieldBlue.clear();
            exptimeTextFieldBlue.setEditable(false);
            framerateTextFieldBlue.clear();
            framerateTextFieldBlue.setEditable(false);
            reconstructToggleBlue.setSelected(false);           
            reconstructToggleBlue.setDisable(true);
            frameselectToggleBlue.setSelected(false);           
            frameselectToggleBlue.setDisable(true);
            numacqimsTextFieldBlue.clear();
            numacqimsTextFieldBlue.setEditable(false);
            numselimsTextFieldBlue.clear();
            fieldsampleTextFieldBlue.clear();
            roiTextFieldBlue.clear();
            roiTextFieldBlue.setEditable(false);
            binningTextFieldBlue.clear();
            binningTextFieldBlue.setEditable(false);
            snrTextFieldBlue.clear();
            durationTextFieldBlue.clear();
            coronalUseToggleBlue.setSelected(false);
            coronalUseToggleBlue.setDisable(true);
            relCoronalFluxTextFieldBlue.clear();
            relCoronalFluxTextFieldBlue.setEditable(false);
            relCoronalBackgroundTextFieldBlue.clear();
            relCoronalBackgroundTextFieldBlue.setEditable(false);
            rbAllBlue.setSelected(false);
            rbCenterBlue.setSelected(false);
            rbCustomBlue.setSelected(false);

            // reset field sampling group
            fieldSelectGroupBlue.getChildren().clear();

            // reset line graph
            lineChartBlue.getData().clear();
        }
    }
    /**
     * Fills all text fields to show details about the DSP.
     * If the specified DSP is null, all text fields are cleared.
     * 
     * @param item the dsp or null
     */
    private void showDspDetailsRed() {
        Dsp dsp = null;

        if (displayedItemRed instanceof Dsp) {
            dsp = (Dsp)displayedItemRed;
        }

        if (dsp != null) {
            // set certain text fields editable etc. when necessary.
            if (dsp.getIsLib() == true) {
                framerateTextFieldRed.setEditable(false);
                exptimeTextFieldRed.setEditable(false);
                roiTextFieldRed.setEditable(false);
                binningTextFieldRed.setEditable(false);
                reconstructToggleRed.setDisable(true);
                frameselectToggleRed.setDisable(true);
                numacqimsTextFieldRed.setEditable(false);
                numselimsTextFieldRed.setEditable(false);
                coronalUseToggleRed.setDisable(true);
                relCoronalFluxTextFieldRed.setEditable(false);
                relCoronalBackgroundTextFieldRed.setEditable(false);
            } else {
                if (appIpc.getExpertMode()) {
                  framerateTextFieldRed.setEditable(true);
                  if (!(dsp.getSpeckleProcessing())) {
                    roiTextFieldRed.setEditable(true);
                    binningTextFieldRed.setEditable(true);
                  }
                  if (dsp.getIsCoronalWavelength()) {
                      coronalUseToggleRed.setDisable(false);
                  } else {
                      coronalUseToggleRed.setDisable(true);
                  }
                }
                exptimeTextFieldRed.setEditable(true);
                reconstructToggleRed.setDisable(false);
                frameselectToggleRed.setDisable(false);
                numacqimsTextFieldRed.setEditable(true);
            }
            // the following line is special for RED
            if (dsp.getNumActiveFieldSamples() == 4) {
                tgFieldSelectRed.selectToggle(rbAllRed);
            } else if ((dsp.getNumActiveFieldSamples() == 1) &&
                       (dsp.getFieldSamples(4).get())) {
                tgFieldSelectRed.selectToggle(rbCenterRed);
            } else {
                // have to set alFieldSelect properly
                for (int i=0; i<Dsp.NUMFIELDS; i++) {
                    alFieldSelectRed.set(i, dsp.getFieldSamples(i).get());
                }
                tgFieldSelectRed.selectToggle(rbCustomRed);
            }
            // disable ROI/bin input fields if in expert mode and more than
            // one field sample is selected, otherwise enable
            if (dsp.getNumActiveFieldSamples() == 1) {
                if (appIpc.getExpertMode() && !(dsp.getSpeckleProcessing())) {
                    roiTextFieldRed.setEditable(true);
                    binningTextFieldRed.setEditable(true);
                }
            } else {
                if (appIpc.getExpertMode() && !(dsp.getSpeckleProcessing())) {
                    roiTextFieldRed.setEditable(false);
                    binningTextFieldRed.setEditable(false);
                }
            }

            // Fill the labels with info from the Dsp object.
            dspNameTextFieldRed.setText(dsp.getItemName());
            wavelengthTextFieldRed.setText(Float.toString(dsp.getWaveLength()));
            exptimeTextFieldRed.setText(Float.toString(dsp.getExpTime()));
            reconstructToggleRed.setSelected(dsp.getSpeckleProcessing());
            frameselectToggleRed.setSelected(dsp.getFrameSelection());
            numacqimsTextFieldRed.setText(Integer.toString(dsp.getNumAcqImages()));
            numselimsTextFieldRed.setText(Integer.toString(dsp.getNumSelImages()));
            framerateTextFieldRed.setText(Float.toString(dsp.getFrameRate()));
            fieldsampleTextFieldRed.setText(Integer.toString(dsp.getNumActiveFieldSamples()));
            roiTextFieldRed.setText(convertROIInfoToText(dsp));
            binningTextFieldRed.setText(Integer.toString(dsp.getBinFactor()));
            double photons;
            double noise;
            if (dsp.getRelCoronalFlux() > 0.0) {
              photons = dsp.getPhotonsPerSecond((float)(PIXELFOVRED*Math.pow(dsp.getBinFactor(), 2)), 0, true) *
                        dsp.getExpTime()*1e-3 * dsp.getRelCoronalFlux()*1e-6;
              noise = Math.sqrt(dsp.getPhotonsPerSecond((float)(PIXELFOVRED*Math.pow(dsp.getBinFactor(), 2)), 0, false) *
                      dsp.getExpTime()*1e-3  * Math.abs(dsp.getRelCoronalBackground())*1e-6 + photons + 
                      Math.pow(Dsp.CAMREADNOISE*dsp.getBinFactor(), 2));
              snrTextFieldRed.setText(Integer.toString((int)roundToDecimal((float)(photons/noise),0)));
            } else {
              photons = dsp.getPhotonsPerSecond((float)(PIXELFOVRED*Math.pow(dsp.getBinFactor(), 2)),
                        appIpc.getMu(), false) * dsp.getExpTime()*1e-3;
              snrTextFieldRed.setText(Integer.toString((int)roundToDecimal((float)Math.sqrt(photons),0)));
            }
            durationTextFieldRed.setText(Float.toString(roundToDecimal(dsp.getRawDuration(), 3)));
            if (!dsp.getIsLib() & dsp.getIsCoronalWavelength() & appIpc.getExpertMode()) {
              if (dsp.getRelCoronalFlux() > 0.0) {
                relCoronalFluxTextFieldRed.setText(Float.toString(dsp.getRelCoronalFlux()));
                relCoronalFluxTextFieldRed.setEditable(true);
                relCoronalBackgroundTextFieldRed.setText(Float.toString(dsp.getRelCoronalBackground()));
                relCoronalBackgroundTextFieldRed.setEditable(true);
                coronalUseToggleRed.setSelected(true);
              }
            } else {
              relCoronalFluxTextFieldRed.clear();
              relCoronalFluxTextFieldRed.setEditable(false);
              relCoronalBackgroundTextFieldRed.clear();
              relCoronalBackgroundTextFieldRed.setEditable(false);
              coronalUseToggleRed.setSelected(false);
            }
            // display field sample overlay
            displayFieldSampleOverlay(dsp, fieldSelectGroupRed, sampleImageViewRed, alFieldSelectRed, true);

            // update line graph
            lineChartRed.getData().clear();
            lineChartRed.getData().add(setLineChartData(dsp, "spectrum"));
            lineChartRed.getData().add(setLineChartData(dsp, "profile"));
        } else {
            // Dsp is null, remove all the text.
            dspNameTextFieldRed.clear();
            wavelengthTextFieldRed.clear();
            exptimeTextFieldRed.clear();
            exptimeTextFieldRed.setEditable(false);
            framerateTextFieldRed.clear();
            framerateTextFieldRed.setEditable(false);
            reconstructToggleRed.setSelected(false);           
            reconstructToggleRed.setDisable(true);
            frameselectToggleRed.setSelected(false);           
            frameselectToggleRed.setDisable(true);
            numacqimsTextFieldRed.clear();
            numacqimsTextFieldRed.setEditable(false);
            numselimsTextFieldRed.clear();
            fieldsampleTextFieldRed.clear();
            roiTextFieldRed.clear();
            roiTextFieldRed.setEditable(false);
            binningTextFieldRed.clear();
            binningTextFieldRed.setEditable(false);
            snrTextFieldRed.clear();
            durationTextFieldRed.clear();
            coronalUseToggleRed.setSelected(false);
            coronalUseToggleRed.setDisable(true);
            relCoronalFluxTextFieldRed.clear();
            relCoronalFluxTextFieldRed.setEditable(false);
            relCoronalBackgroundTextFieldRed.clear();
            relCoronalBackgroundTextFieldRed.setEditable(false);
            rbAllRed.setSelected(false);
            rbCenterRed.setSelected(false);
            rbCustomRed.setSelected(false);

            // reset field sampling group
            fieldSelectGroupRed.getChildren().clear();

            // reset line graph
            lineChartRed.getData().clear();
        }
    }

    private void showSummaryInfoBlue() {
        // update the summary labels
        float totDur  = dspTreeViewBlue.getRoot().getValue().getRawDuration();
        int   seqIter = ((DspGrp)(dspTreeViewBlue.getRoot().getValue())).getNumCycles();
        float seqDur  = totDur/seqIter;
        float dataVol = dspTreeViewBlue.getRoot().getValue().getDataVolume();
        float pDRate  = dspTreeViewBlue.getRoot().getValue().getPeakDataRate();
        totalDurationBlue.setText(generateHumanReadableTime(totDur));
        seqDurationBlue.setText(generateHumanReadableTime(seqDur));
        avgDataRateBlue.setText(Float.toString(dataVol/((float)Math.pow(1024, 2))/totDur));
        dataVolumeBlue.setText(Float.toString(dataVol/((float)Math.pow(1024, 3))));
        peakDataRateBlue.setText(Float.toString(pDRate/(float)Math.pow(1024, 2)));
        
        ObservableList<Dsp> list = dspTreeViewBlue.getRoot().getValue().getFlattenedTreeItems();
        
        pTimeLineBlue.getChildren().clear();
        double currentWidth = 0.0;
        for (int i=0; i<list.size(); i++) {
            Rectangle rB = new Rectangle();
            rB.setFill(Color.hsb((double)list.get(i).getWaveLength()*Math.PI, 1.0, 1.0));
            rB.setStroke(Color.BLACK);
            rB.setX(currentWidth);
            rB.widthProperty().bind(pTimeLineBlue.widthProperty().multiply(list.get(i).getRawDuration()).divide(seqDur));
            rB.heightProperty().bind(pTimeLineBlue.heightProperty());
            Tooltip tB = new Tooltip(list.get(i).getItemName()+": "+generateHumanReadableTime(list.get(i).getRawDuration()));
            tB.setAutoHide(true);
            Tooltip.install(rB, tB);
            currentWidth += rB.getWidth();
            pTimeLineBlue.getChildren().add(rB);
        }
        
        // update Synchronization details
        if (tgSyncSelection.getSelectedToggle() != null) {
            synchronizeAndUpdateDspTreeItems(tgSyncSelection.getSelectedToggle());
        }
    }
    private void showSummaryInfoRed() {
        // update the summary labels
        float totDur  = dspTreeViewRed.getRoot().getValue().getRawDuration();
        int   seqIter = ((DspGrp)(dspTreeViewRed.getRoot().getValue())).getNumCycles();
        float seqDur  = totDur/seqIter;
        float dataVol = dspTreeViewRed.getRoot().getValue().getDataVolume();
        float pDRate  = dspTreeViewRed.getRoot().getValue().getPeakDataRate();
        totalDurationRed.setText(generateHumanReadableTime(totDur));
        seqDurationRed.setText(generateHumanReadableTime(seqDur));
        avgDataRateRed.setText(Float.toString(dataVol/((float)Math.pow(1024, 2))/totDur));
        dataVolumeRed.setText(Float.toString(dataVol/((float)Math.pow(1024, 3))));
        peakDataRateRed.setText(Float.toString(pDRate/(float)Math.pow(1024, 2)));
        
        ObservableList<Dsp> list = dspTreeViewRed.getRoot().getValue().getFlattenedTreeItems();
        
        pTimeLineRed.getChildren().clear();
        double currentWidth = 0.0;
        for (int i=0; i<list.size(); i++) {
            Rectangle rB = new Rectangle();
            rB.setFill(Color.hsb((double)list.get(i).getWaveLength()*Math.PI, 1.0, 1.0));
            rB.setStroke(Color.BLACK);
            rB.setX(currentWidth);
            rB.widthProperty().bind(pTimeLineRed.widthProperty().multiply(list.get(i).getRawDuration()).divide(seqDur));
            rB.heightProperty().bind(pTimeLineRed.heightProperty());
            Tooltip tB = new Tooltip(list.get(i).getItemName()+": "+generateHumanReadableTime(list.get(i).getRawDuration()));
            tB.setAutoHide(true);
            Tooltip.install(rB, tB);
            currentWidth += rB.getWidth();
            pTimeLineRed.getChildren().add(rB);
        }
        
        // update Synchronization details
        if (tgSyncSelection.getSelectedToggle() != null) {
            synchronizeAndUpdateDspTreeItems(tgSyncSelection.getSelectedToggle());
        }
    }

    /**
     * Functions to check user input
     */
    // BLUE
    @FXML
    private void checkExpTimeInputBlue() {
        try {
          if (displayedItemBlue instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemBlue;
            // Check exposure time compatibility with current frame rate and full well
            dsp.setExpTime(
                dsp.checkExposureTimeVsFrameRate(
                    dsp.checkExposureTimeVsFullWell(Float.parseFloat(exptimeTextFieldBlue.getText()), (float) PIXELFOVBLUE, appIpc.getMu())
                )
            );
            logMessages.appendText(
                "** VBI Blue DSP "+dsp.getItemName()+" :: "
                +"Exposure time :: "
                +"Input: "+exptimeTextFieldBlue.getText()+" -- "
                +"Adjusted: "+dsp.getExpTime()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewBlue.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsBlue();
            // update Summary details
            showSummaryInfoBlue();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkNumFramesInputBlue() {
        try {
          if (displayedItemBlue instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemBlue;
            dsp.setSpeckleProcessing(reconstructToggleBlue.isSelected());
            dsp.setFrameSelection(frameselectToggleBlue.isSelected());
            // Check whether the logic for the number of images is correct
            int[] goodVals = dsp.checkNumImages(
                Integer.parseInt(numselimsTextFieldBlue.getText()),
                Integer.parseInt(numacqimsTextFieldBlue.getText())
            );
            dsp.setNumSelImages(goodVals[0]);
            dsp.setNumAcqImages(goodVals[1]);
            logMessages.appendText(
                "** VBI Blue DSP "+dsp.getItemName()+" :: "
                +"Selected/Acquired Frames :: "
                +"Input: "+numselimsTextFieldBlue.getText()+"/"+numacqimsTextFieldBlue.getText()+" -- "
                +"Adjusted: "+dsp.getNumSelImages()+"/"+dsp.getNumAcqImages()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewBlue.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsBlue();
            // update Summary details
            showSummaryInfoBlue();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkFrameRateInputBlue() {
        try {
          if (displayedItemBlue instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemBlue;
            // set Frame Rate
            dsp.setFrameRate(dsp.checkFrameRate(Float.parseFloat(framerateTextFieldBlue.getText())));
            logMessages.appendText(
                "** VBI Blue DSP "+dsp.getItemName()+" :: "
                +"Frame Rate :: "
                +"Input: "+framerateTextFieldBlue.getText()+" -- "
                +"Adjusted: "+dsp.getFrameRate()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewBlue.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsBlue();
            // update Summary details
            showSummaryInfoBlue();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkROIInputBlue() {
        try {
          if (displayedItemBlue instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemBlue;
            // prepare the String for checking
            String roiText = roiTextFieldBlue.getText();
            String[] roiTextSplit = roiText.split("[ ]*,[ ]*");
            for (int i=0; i<Dsp.NUMROIPARAMS; i++) {
                dsp.setRoiParams(i, new SimpleIntegerProperty(dsp.checkROIValue(roiTextSplit)[i]));
            }
            logMessages.appendText(
                "** VBI Blue DSP "+dsp.getItemName()+" :: "
                +"ROI :: "
                +"Input: "+roiText+" -- "
                +"Adjusted: "+dsp.getRoiParams(0).get()+","+dsp.getRoiParams(1).get()+","+dsp.getRoiParams(2).get()+","+dsp.getRoiParams(3).get()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewBlue.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsBlue();
            // update Summary details
            showSummaryInfoBlue();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkBinInputBlue() {
        try {
          if (displayedItemBlue instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemBlue;
            // set Binning
            dsp.setBinFactor(dsp.checkBinningFactor(Integer.parseInt(binningTextFieldBlue.getText())));
            logMessages.appendText(
                "** VBI Blue DSP "+dsp.getItemName()+" :: "
                +"Binning :: "
                +"Input: "+binningTextFieldBlue.getText()+" -- "
                +"Adjusted: "+dsp.getBinFactor()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewBlue.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsBlue();
            // update Summary details
            showSummaryInfoBlue();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkSpeckleInputBlue() {
        try {
          if (displayedItemBlue instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemBlue;
            checkNumFramesInputBlue();
            checkROIInputBlue();
            checkBinInputBlue();
            checkFrameRateInputBlue();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkRelCoronalFluxInputBlue() {
      try {
        if (displayedItemBlue instanceof Dsp) {
          Dsp dsp = (Dsp)displayedItemBlue;
          if (relCoronalFluxTextFieldBlue.getText().isEmpty()) {
            dsp.setRelCoronalFlux(0);
          } else {
            dsp.setRelCoronalFlux(Float.parseFloat(relCoronalFluxTextFieldBlue.getText()));
          }
          // need to check if speckle was deselected, with only one DSP and field
          // -- removes moveTime
          dspTreeViewBlue.getRoot().getValue().checkTreeItems();
          // update Settings details
          showDspDetailsBlue();
          // update Summary details
          showSummaryInfoBlue();
        }
      } catch (NullPointerException e) {
      }
    }
    @FXML
    private void checkRelCoronalBackgroundInputBlue() {
      try {
        if (displayedItemBlue instanceof Dsp) {
          Dsp dsp = (Dsp)displayedItemBlue;
          if (relCoronalBackgroundTextFieldBlue.getText().isEmpty()) {
            dsp.setRelCoronalBackground(0);
          } else {
            dsp.setRelCoronalBackground(Float.parseFloat(relCoronalBackgroundTextFieldBlue.getText()));
          }
          // need to check if speckle was deselected, with only one DSP and field
          // -- removes moveTime
          dspTreeViewBlue.getRoot().getValue().checkTreeItems();
          // update Settings details
          showDspDetailsBlue();
          // update Summary details
          showSummaryInfoBlue();
        }
      } catch (NullPointerException e) {
      }
    }
    // RED
    @FXML
    private void checkExpTimeInputRed() {
        try {
          if (displayedItemRed instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemRed;
              // Check exposure time compatibility with current frame rate and full well
            dsp.setExpTime(
                dsp.checkExposureTimeVsFrameRate(
                    dsp.checkExposureTimeVsFullWell(Float.parseFloat(exptimeTextFieldRed.getText()), (float) PIXELFOVRED, appIpc.getMu())
                )
            );
            logMessages.appendText(
                "** VBI Red DSP "+dsp.getItemName()+" :: "
                +"Exposure time :: "
                +"Input: "+exptimeTextFieldRed.getText()+" -- "
                +"Adjusted: "+dsp.getExpTime()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewRed.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsRed();
            // update Summary details
            showSummaryInfoRed();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkNumFramesInputRed() {
        try {
          if (displayedItemRed instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemRed;
            dsp.setSpeckleProcessing(reconstructToggleRed.isSelected());
            dsp.setFrameSelection(frameselectToggleRed.isSelected());
            // Check whether the logic for the number of images is correct
            int[] goodVals = dsp.checkNumImages(
                Integer.parseInt(numselimsTextFieldRed.getText()),
                Integer.parseInt(numacqimsTextFieldRed.getText())
            );
            dsp.setNumSelImages(goodVals[0]);
            dsp.setNumAcqImages(goodVals[1]);
            logMessages.appendText(
                "** VBI Red DSP "+dsp.getItemName()+" :: "
                +"Selected/Acquired Frames :: "
                +"Input: "+numselimsTextFieldRed.getText()+"/"+numacqimsTextFieldRed.getText()+" -- "
                +"Adjusted: "+dsp.getNumSelImages()+"/"+dsp.getNumAcqImages()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewRed.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsRed();
            // update Summary details
            showSummaryInfoRed();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkFrameRateInputRed() {
        try {
          if (displayedItemRed instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemRed;
            // set Frame Rate
            dsp.setFrameRate(dsp.checkFrameRate(Float.parseFloat(framerateTextFieldRed.getText())));
            logMessages.appendText(
                "** VBI Red DSP "+dsp.getItemName()+" :: "
                +"Frame Rate :: "
                +"Input: "+framerateTextFieldRed.getText()+" -- "
                +"Adjusted: "+dsp.getFrameRate()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewBlue.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsRed();
            // update Summary details
            showSummaryInfoRed();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkROIInputRed() {
        try {
          if (displayedItemRed instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemRed;
            // prepare the String for checking
            String roiText = roiTextFieldRed.getText();
            String[] roiTextSplit = roiText.split("[ ]*,[ ]*");
            for (int i=0; i<Dsp.NUMROIPARAMS; i++) {
                dsp.setRoiParams(i, new SimpleIntegerProperty(dsp.checkROIValue(roiTextSplit)[i]));
            }
            logMessages.appendText(
                "** VBI Red DSP "+dsp.getItemName()+" :: "
                +"ROI :: "
                +"Input: "+roiText+" -- "
                +"Adjusted: "+dsp.getRoiParams(0).get()+","+dsp.getRoiParams(1).get()+","+dsp.getRoiParams(2).get()+","+dsp.getRoiParams(3).get()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewRed.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsRed();
            // update Summary details
            showSummaryInfoRed();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkBinInputRed() {
        try {
          if (displayedItemRed instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemRed;
            // set Binning
            dsp.setBinFactor(dsp.checkBinningFactor(Integer.parseInt(binningTextFieldRed.getText())));
            logMessages.appendText(
                "** VBI Red DSP "+dsp.getItemName()+" :: "
                +"Binning :: "
                +"Input: "+binningTextFieldRed.getText()+" -- "
                +"Adjusted: "+dsp.getBinFactor()+"\n"
            );
            // need to check if speckle was deselected, with only one DSP and field
            // -- removes moveTime
            dspTreeViewRed.getRoot().getValue().checkTreeItems();
            // update Settings details
            showDspDetailsRed();
            // update Summary details
            showSummaryInfoRed();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkSpeckleInputRed() {
        try {
          if (displayedItemRed instanceof Dsp) {
            Dsp dsp = (Dsp)displayedItemRed;
            checkNumFramesInputRed();
            checkROIInputRed();
            checkBinInputRed();
            checkFrameRateInputRed();
          }
        } catch (NullPointerException e) {
        }
    }
    @FXML
    private void checkRelCoronalFluxInputRed() {
      try {
        if (displayedItemRed instanceof Dsp) {
          Dsp dsp = (Dsp)displayedItemRed;
          if (relCoronalFluxTextFieldRed.getText().isEmpty()) {
            dsp.setRelCoronalFlux(0);
          } else {
            dsp.setRelCoronalFlux(Float.parseFloat(relCoronalFluxTextFieldRed.getText()));
          }
          // need to check if speckle was deselected, with only one DSP and field
          // -- removes moveTime
          dspTreeViewRed.getRoot().getValue().checkTreeItems();
          // update Settings details
          showDspDetailsRed();
          // update Summary details
          showSummaryInfoRed();
        }
      } catch (NullPointerException e) {
      }
    }
    @FXML
    private void checkRelCoronalBackgroundInputRed() {
      try {
        if (displayedItemRed instanceof Dsp) {
          Dsp dsp = (Dsp)displayedItemRed;
          if (relCoronalBackgroundTextFieldRed.getText().isEmpty()) {
            dsp.setRelCoronalBackground(0);
          } else {
            dsp.setRelCoronalBackground(Float.parseFloat(relCoronalBackgroundTextFieldRed.getText()));
          }
          // need to check if speckle was deselected, with only one DSP and field
          // -- removes moveTime
          dspTreeViewRed.getRoot().getValue().checkTreeItems();
          // update Settings details
          showDspDetailsRed();
          // update Summary details
          showSummaryInfoRed();
        }
      } catch (NullPointerException e) {
      }
    }
    
    /**
     * Called when the user clicks on field selection.
     * 
     * because the apply button only works with the actual built up editable 
     * DSP tree, it is ok to work off of its selected item
     */
    private void handleFieldSelectBlue() {
        try {
          if (displayedItemBlue instanceof Dsp) {
              Dsp dsp = (Dsp)displayedItemBlue;
              for (int i=0; i<Dsp.NUMFIELDS; i++) {
                  dsp.setFieldSamples(i, new SimpleBooleanProperty((boolean)alFieldSelectBlue.get(i)));
              }
              // ROI is only accepted when a single field is selected.
              // need to recheck this in case something changed with the field selection
              String roiText = roiTextFieldBlue.getText();
              String[] roiTextSplit = roiText.split("[ ]*,[ ]*");
              for (int i=0; i<Dsp.NUMROIPARAMS; i++) {
                  dsp.setRoiParams(i, new SimpleIntegerProperty(dsp.checkROIValue(roiTextSplit)[i]));
              }
              // need to verify the frame rate is still good now
              if (!framerateTextFieldBlue.getText().isEmpty()) {
                  dsp.setFrameRate(dsp.checkFrameRate(Float.parseFloat(framerateTextFieldBlue.getText())));
              }
              // need to check if someone went from single to full field, with only one DSP
              // -- adds moveTime
              dsp.checkTreeItems();
          }
          // need to check if full multiple fields were reduced to single field, with only one DSP
          // -- removes moveTime
          dspTreeViewBlue.getRoot().getValue().checkTreeItems();
          showDspDetailsBlue();
          showSummaryInfoBlue();
        } catch (NullPointerException e) {
        }
    }
    /**
     * Called when the user clicks Apply.
     * 
     * because the apply button only works with the actual built up editable 
     * DSP tree, it is ok to work off of its selected item
     */
    private void handleFieldSelectRed() {
        try {
          if (displayedItemRed instanceof Dsp) {
              Dsp dsp = (Dsp)displayedItemRed;
              for (int i=0; i<Dsp.NUMFIELDS; i++) {
                  dsp.setFieldSamples(i, new SimpleBooleanProperty((boolean)alFieldSelectRed.get(i)));
              }
              // ROI is only accepted when a single field is selected.
              // need to recheck this in case something changed with the field selection
              String roiText = roiTextFieldRed.getText();
              String[] roiTextSplit = roiText.split("[ ]*,[ ]*");
              for (int i=0; i<Dsp.NUMROIPARAMS; i++) {
                  dsp.setRoiParams(i, new SimpleIntegerProperty(dsp.checkROIValue(roiTextSplit)[i]));
              }
              // need to verify the frame rate is still good now
              if (!framerateTextFieldRed.getText().isEmpty()) {
                  dsp.setFrameRate(dsp.checkFrameRate(Float.parseFloat(framerateTextFieldRed.getText())));
              }
              // need to check if someone went from single to full field, with only one DSP
              // -- adds moveTime
              dsp.checkTreeItems();
          }
          // need to check if full multiple fields were reduced to single field, with only one DSP
          // -- removes moveTime
          dspTreeViewBlue.getRoot().getValue().checkTreeItems();
          showDspDetailsRed();
          showSummaryInfoRed();
        } catch (NullPointerException e) {
        }
    }

    /**
     * Create a line plot
     * @param dsp The DSP information
     * @param dataType The type of data (spectrum or filter profile) too display
     */
    private XYChart.Series setLineChartData(Dsp dsp, String dataType) {
        XYChart.Series series = new XYChart.Series<>();
        
        Double maxSpec = Collections.max(dsp.getFluxInfo().getSpectrum());
        Double maxIntFil = Collections.max(dsp.getFluxInfo().getIntFil());

        if (dataType.equals("spectrum")) {
            for (int i=0; i<dsp.getFluxInfo().getWavelength().size(); i+=10) {
                series.getData().add(new XYChart.Data<>(dsp.getFluxInfo().getWavelength().get(i), dsp.getFluxInfo().getSpectrum().get(i)));
            }
        }
        if (dataType.equals("profile")) {
            for (int i=0; i<dsp.getFluxInfo().getWavelength().size(); i+=10) {
                series.getData().add(new XYChart.Data<>(dsp.getFluxInfo().getWavelength().get(i), dsp.getFluxInfo().getIntFil().get(i)*maxSpec/maxIntFil));
            }
        }

        return series;
    }
    
    /**
     * 
     * @param dsp The DSP information
     */
    private void displayFieldSampleOverlay(Dsp dsp, Group fsGroup, ImageView imView, ArrayList alFS, boolean red) {
        if (dsp != null) {
            for (int i=0; i<Dsp.NUMFIELDS; i++) alFS.set(i, dsp.getFieldSamples(i).get());
            fsGroup.getChildren().clear();
            fsGroup.getChildren().add(imView);
            fsGroup.getChildren().add(createFieldSampleOverlay(imView, alFS, red));
        } else {
            fsGroup.getChildren().clear();
            fsGroup.getChildren().add(imView);
            fsGroup.getChildren().add(createFieldSampleOverlay(imView, alFS, red));
        }
    }
    
    /**
     * create an overlay mask to put over image to indicate what fields are part
     * of the Field sampling. Is modified interactively.
     */
    private Shape createFieldSampleOverlay(ImageView imView, ArrayList alFS, boolean red) {
        double w = imView.getFitWidth();
        double h = imView.getFitHeight();
        Shape overlay = new Rectangle(0, 0, w, h);

        if (red) {
            if (alFS.get(4).equals(false)) {
                for (int i=0; i<Dsp.NUMFIELDS; i++) {
                    if (alFS.get(i).equals(true)) {
                        Rectangle ot = new Rectangle((int)i%2*w/2,(int)i/2*h/2,w/2,h/2);
                        overlay = Shape.subtract(overlay, ot);
                    }
                }
            } else {
                Rectangle ot = new Rectangle((int)w/4,(int)h/4,w/2,h/2);
                overlay = Shape.subtract(overlay, ot);
            }
        } else {
            for (int i=0; i<Dsp.NUMFIELDS; i++) {
                if (alFS.get(i).equals(true)) {
                    Rectangle ot = new Rectangle((int)i%3*w/3,(int)i/3*h/3,w/3,h/3);
                    overlay = Shape.subtract(overlay, ot);
                }
            }
        }

        overlay.setOpacity(0.5);
        overlay.setFill(Color.BLACK);
        return overlay;
    }

    /**
     * @param dsp The DSP information
     * @return String with ROI information for display
     */
    private String convertROIInfoToText(Dsp dsp) {
        String roiText = "";
        roiText = roiText.concat(Integer.toString(dsp.getRoiParams(0).get()));
        for (int i=1; i<Dsp.NUMROIPARAMS; i++) {
            roiText = roiText.concat(",");
            roiText = roiText.concat(Integer.toString(dsp.getRoiParams(i).get()));
        }
        return roiText;
    }
    
    /**
     * Generate human readable time from seconds
     * @param time - input time in seconds
     * @return - String with human readable time
     */
    private String generateHumanReadableTime(float time) {
        String output;
        int minutes = 0;
        int hours = 0;
        if (time >= 60) {
            minutes = (int)time / (int)60;
        }
        if (minutes >= 60) {
            hours = (int)minutes / (int)60;
        }
        if (hours == 0) {
            if (minutes == 0) {
                output = Float.toString(roundToDecimal(time%60, 3)) + " s";
            } else {
                output = Integer.toString(minutes) + " m " 
                       + Float.toString(roundToDecimal(time%60, 3)) + " s";
            }
        } else {
            output = Integer.toString(hours) + " h "
                   + Integer.toString(minutes%60) + " m "
                   + Float.toString(roundToDecimal(time%60, 3)) + " s";
        }
        return output;
    }
    
    /**
     * Generate a decimal rounded to desired number of digits
     * @param d - number to round
     * @param decimalPlace - decimal place to round to
     * @return - decimal rounded to the input decimal place
     */
    private float roundToDecimal(float d, int decimalPlace) {
        double scale = Math.pow(10, decimalPlace);
        return (float)(Math.round((double)d * scale) / scale);
    }

    
    /**
     * 
     * @param syncSelection 
     */
    private void synchronizeAndUpdateDspTreeItems(Toggle syncSelection) {
        // convert selection to integer flag
        // 0: None (default)
        // 1: Loose
        // 2: Fixed
        int syncType = 0;
        if (syncSelection.equals(rbSyncLoose)) syncType = 1;
        if (syncSelection.equals(rbSyncFixed)) syncType = 2;
        // adjust the syncSel indicator in appIpc
        appIpc.setSyncSel(syncType);
        
        // clear everything
        pSyncTimeLineBlue.getChildren().clear();
        double currentWidthBlue = 0.0;
        pSyncTimeLineRed.getChildren().clear();
        double currentWidthRed = 0.0;

        // check first if lists are populated
        boolean blueFlag;
        ObservableList<Dsp> listBlue = dspTreeViewBlue.getRoot().getValue().getFlattenedTreeItems();
        blueFlag = listBlue.size() >= 1;
        boolean redFlag;
        ObservableList<Dsp> listRed = dspTreeViewRed.getRoot().getValue().getFlattenedTreeItems();
        redFlag = listRed.size() >= 1;

        // synchronize both channels according to the syncType Flag
        // BLUE
        if (blueFlag)
            dspTreeViewBlue.getRoot().getValue().synchWithDspTreeItem(null, syncType);
        // RED
        if (redFlag) 
            dspTreeViewRed.getRoot().getValue().synchWithDspTreeItem(null, syncType);
        // RED & BLUE
        if (blueFlag && redFlag) {
           dspTreeViewBlue.getRoot().getValue().synchWithDspTreeItem(dspTreeViewRed.getRoot().getValue(), syncType);
        }

        // >>> BLUE
        float totDurBlue  = dspTreeViewBlue.getRoot().getValue().getSyncDuration();
        int   seqIterBlue = ((DspGrp)(dspTreeViewBlue.getRoot().getValue())).getNumCycles();
        float seqDurBlue  = totDurBlue/seqIterBlue + dspTreeViewBlue.getRoot().getValue().getWaitTime();
        // >>> RED
        float totDurRed  = dspTreeViewRed.getRoot().getValue().getSyncDuration();
        int   seqIterRed = ((DspGrp)(dspTreeViewRed.getRoot().getValue())).getNumCycles();
        float seqDurRed  = totDurRed/seqIterRed + dspTreeViewRed.getRoot().getValue().getWaitTime();
        // need the maximum durations
        float seqDur = (seqDurBlue > seqDurRed) ? seqDurBlue : seqDurRed;
        float totDur = (totDurBlue > totDurRed) ? totDurBlue : totDurRed;

        // >>>>> BLUE
        int cSeqIter = 0;
        float cSeqDur = 0;
        // add more sequences if needed
        while ((cSeqIter < seqIterBlue) && (cSeqDur < seqDur)) {
            for (int i=0; i<listBlue.size(); i++) {
                Dsp temp = listBlue.get(i);

                Rectangle rB1 = new Rectangle();
                rB1.setFill(Color.hsb((double)temp.getWaveLength()*Math.PI, 1.0, 1.0));
                rB1.setStroke(Color.GREY);
                // add additional DSPs in further cycles as 'ghost' cycles
                if (cSeqIter > 0) rB1.setOpacity(0.25);
                rB1.setX(currentWidthBlue);
                // do this to add 'ghost' DSPs in further cycles until the time line is full
                if (cSeqDur + temp.getRawDuration() <= seqDur) {
                    rB1.widthProperty().bind(pSyncTimeLineBlue.widthProperty().multiply(temp.getRawDuration()).divide(seqDur));
                } else {
                    rB1.widthProperty().bind(pSyncTimeLineBlue.widthProperty().multiply(seqDur-cSeqDur).divide(seqDur));
                }
                rB1.heightProperty().bind(pSyncTimeLineBlue.heightProperty());
                Tooltip tB1 = new Tooltip(temp.getItemName()+": "+generateHumanReadableTime(temp.getRawDuration()));
                tB1.setAutoHide(true);
                Tooltip.install(rB1, tB1);
                currentWidthBlue += rB1.getWidth();
                pSyncTimeLineBlue.getChildren().add(rB1);
                cSeqDur += temp.getRawDuration();
                if (temp.getWaitTime() != 0.0) {
                    Rectangle rB2 = new Rectangle();
                    rB2.setFill(Color.LIGHTGREY);
                    rB2.setStroke(Color.GREY);
                    // add additional DSPs in further cycles as 'ghost' cycles
                    if (cSeqIter > 0) rB2.setOpacity(0.25);
                    rB2.setX(currentWidthBlue);
                    // do this to add 'ghost' wait times in further cycles until the time line is full
                    if (cSeqDur + temp.getWaitTime() <= seqDur) {
                        rB2.widthProperty().bind(pSyncTimeLineBlue.widthProperty().multiply(temp.getWaitTime()).divide(seqDur));
                    } else {
                        rB2.widthProperty().bind(pSyncTimeLineBlue.widthProperty().multiply(seqDur-cSeqDur).divide(seqDur));
                    }
                    rB2.heightProperty().bind(pSyncTimeLineBlue.heightProperty());
                    Tooltip tB2 = new Tooltip("Wait: "+generateHumanReadableTime(temp.getWaitTime()));
                    tB2.setAutoHide(true);
                    Tooltip.install(rB2, tB2);
                    currentWidthBlue += rB2.getWidth();
                    pSyncTimeLineBlue.getChildren().add(rB2);
                    cSeqDur += temp.getWaitTime();
                }
                // break if the time line is full
                if (cSeqDur > seqDur) break;
            }
            cSeqIter += 1;
            // do this only once for syncSelection "Loose"
            if (syncType == 1) break;
        }
        // add sequence wait time (only applicable for syncSelection Loose)
        if (dspTreeViewBlue.getRoot().getValue().getWaitTime() != 0.0) {
            Rectangle rB = new Rectangle();
            rB.setFill(Color.LIGHTGREY);
            rB.setStroke(Color.GREY);
            rB.setX(currentWidthBlue);
            rB.widthProperty().bind(pSyncTimeLineBlue.widthProperty().multiply(dspTreeViewBlue.getRoot().getValue().getWaitTime()).divide(seqDur));
            rB.heightProperty().bind(pSyncTimeLineBlue.heightProperty());
            Tooltip tB = new Tooltip("Wait: "+generateHumanReadableTime(dspTreeViewBlue.getRoot().getValue().getWaitTime()));
            tB.setAutoHide(true);
            Tooltip.install(rB, tB);
            currentWidthBlue += rB.getWidth();
            pSyncTimeLineBlue.getChildren().add(rB);
        }

        // >>>>> RED
        cSeqIter = 0;
        cSeqDur = 0;
        // add more sequences if needed
        while ((cSeqIter < seqIterRed) && (cSeqDur < seqDur)) {
            for (int i=0; i<listRed.size(); i++) {
                Dsp temp = listRed.get(i);

                Rectangle rB1 = new Rectangle();
                rB1.setFill(Color.hsb((double)temp.getWaveLength()*Math.PI, 1.0, 1.0));
                rB1.setStroke(Color.GRAY);
                // add additional DSPs in further cycles as 'ghost' cycles
                if (cSeqIter > 0) rB1.setOpacity(0.25);
                rB1.setX(currentWidthRed);
                // do this to add 'ghost' DSPs in further cycles until the time line is full
                if (cSeqDur + temp.getRawDuration() <= seqDur) {
                    rB1.widthProperty().bind(pSyncTimeLineRed.widthProperty().multiply(temp.getRawDuration()).divide(seqDur));
                } else {
                    rB1.widthProperty().bind(pSyncTimeLineRed.widthProperty().multiply(seqDur-cSeqDur).divide(seqDur));
                }
                rB1.heightProperty().bind(pSyncTimeLineRed.heightProperty());
                Tooltip tB1 = new Tooltip(temp.getItemName()+": "+generateHumanReadableTime(temp.getRawDuration()));
                tB1.setAutoHide(true);
                Tooltip.install(rB1, tB1);
                currentWidthRed += rB1.getWidth();
                pSyncTimeLineRed.getChildren().add(rB1);
                cSeqDur += temp.getRawDuration();
                if (temp.getWaitTime() != 0.0) {
                    Rectangle rB2 = new Rectangle();
                    rB2.setFill(Color.LIGHTGREY);
                    rB2.setStroke(Color.GREY);
                    // add additional DSPs in further cycles as 'ghost' cycles
                    if (cSeqIter > 0) rB2.setOpacity(0.25);
                    rB2.setX(currentWidthRed);
                    // do this to add 'ghost' wait times in further cycles until the time line is full
                    if (cSeqDur + temp.getWaitTime() <= seqDur) {
                        rB2.widthProperty().bind(pSyncTimeLineRed.widthProperty().multiply(temp.getWaitTime()).divide(seqDur));
                    } else {
                        rB2.widthProperty().bind(pSyncTimeLineRed.widthProperty().multiply(seqDur-cSeqDur).divide(seqDur));
                    }
                    rB2.heightProperty().bind(pSyncTimeLineRed.heightProperty());
                    Tooltip tB2 = new Tooltip("Wait: "+generateHumanReadableTime(temp.getWaitTime()));
                    tB2.setAutoHide(true);
                    Tooltip.install(rB2, tB2);
                    currentWidthRed += rB2.getWidth();
                    pSyncTimeLineRed.getChildren().add(rB2);
                    cSeqDur += temp.getWaitTime();
                }
                // break if the time line is full
                if (cSeqDur > seqDur) break;
            }
            if (dspTreeViewRed.getRoot().getValue().getWaitTime() != 0.0) {
                Rectangle rB = new Rectangle();
                rB.setFill(Color.LIGHTGREY);
                rB.setStroke(Color.GRAY);
                rB.setX(currentWidthRed);
                rB.widthProperty().bind(pSyncTimeLineRed.widthProperty().multiply(dspTreeViewRed.getRoot().getValue().getWaitTime()).divide(seqDur));
                rB.heightProperty().bind(pSyncTimeLineRed.heightProperty());
                Tooltip tB = new Tooltip("Wait: "+generateHumanReadableTime(dspTreeViewRed.getRoot().getValue().getWaitTime()));
                tB.setAutoHide(true);
                Tooltip.install(rB, tB);
                currentWidthRed += rB.getWidth();
                pSyncTimeLineRed.getChildren().add(rB);
            }
            cSeqIter += 1;
            // do this only once for syncSelection "Loose"
            if (syncType == 1) break;

        }
        // update the summary information
        maxSequenceDuration.setText(generateHumanReadableTime(seqDur));
        totalDuration.setText(generateHumanReadableTime(totDur));
        totalDataVolume.setText(
          String.valueOf(
            (dspTreeViewBlue.getRoot().getValue().getDataVolume() +
             dspTreeViewRed.getRoot().getValue().getDataVolume())/Math.pow(1024, 3)
          ));
    }

    /** 
     * set tree view (recursively)
     * 
     * @param topLevel
     * @param topItem
     */
    public void buildViewFromModel(DspTreeItem topLevel, TreeItem<DspTreeItem> topItem) {
        // this is recursive, assumes that 
        // topLevel corresponds to a DspGrp, and
        // topItem corresponds to the same level of topLevel in the data model
        topItem.setValue(topLevel);
        topItem.setExpanded(true);

        for (int i=0; i<topLevel.getTreeItems().size(); i++) {
            DspTreeItem dspItem = ((DspTreeItem)topLevel.getTreeItems().get(i));
            TreeItem<DspTreeItem> treeItem = new TreeItem<>(dspItem);
            topItem.getChildren().add(treeItem);
            if (dspItem instanceof DspGrp) {
                buildViewFromModel(dspItem, treeItem);
            }
        }
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param appIpc
     */
    public void setIPCApp(ipc appIpc) {
        this.appIpc = appIpc;

        // >>>>> BLUE
        // Add observable list data to the table
        TreeItem<DspTreeItem> prefabRootBlue = new TreeItem<>();
        dspTreeViewPrefabBlue.setRoot(prefabRootBlue);
        buildViewFromModel(appIpc.getDspPrefabRootBlue(), prefabRootBlue);
        // >>>>> RED
        // Add observable list data to the table
        TreeItem<DspTreeItem> prefabRootRed = new TreeItem<>();
        dspTreeViewPrefabRed.setRoot(prefabRootRed);
        buildViewFromModel(appIpc.getDspPrefabRootRed(), prefabRootRed);
        
        // >>>>> BLUE
        // Add items to the TreeView
        TreeItem<DspTreeItem> rootItemBlue = new TreeItem<> ();
        dspTreeViewBlue.setRoot(rootItemBlue);
        buildViewFromModel(appIpc.getTRootBlue(), rootItemBlue);
        // >>>>> RED
        // Add items to the TreeView
        TreeItem<DspTreeItem> rootItemRed = new TreeItem<> ();
        dspTreeViewRed.setRoot(rootItemRed);
        buildViewFromModel(appIpc.getTRootRed(), rootItemRed);
    }
}
